﻿# Shop Parser

It is a shop parser example of a real project. The added supplier is available today and it is possible to start parsing of his real site.

![PHP from Travis config](https://img.shields.io/travis/php-v/symfony/symfony.svg?style=for-the-badge)

## Installation

Clone or download this project to Your site root. Next open schema configuration file `/protected/config/schema-escala.php` and put there Your database identity. If You have no a testing database create it before. Then open site in brauser. Go to *Config* page. You must see information putted to `schema-escala.php`. Press *Install* button. It will create needly tables and fill they required data. Now everything is ready.  
So You can go to *Home* page. Select supplier in dropdown and press *New Products*. It will start parsing process for all supplier products of every category.  

## Description

Briefly, it downloads and parses pages of supplier products, and save products features to MySQL schema.  

This project to get content from Internet uses php function *file_get_contents*, because it more than enough for this project, but in other cases  maybe required *cURL* library or even something more tricky.

Also this parser will work correctly until the markup of supplier product pages still unchanged, but if it will change only need is change appropriate patterns in `Pattern` table (field `Text`).  

Categories are added by hand (it is recommended for security reason, to avoid SQL-injections). Also other security questions are considered at `/protected/components/ware/Parser.php` file (description area).

The most custom code is placed in:  
`/protected/controllers/SiteController.php`  
`/protected/components/ware`  
`/js/script.js`  
`/css/styles.css`  

To keep private commercial information of customer in this example is present only one of his suppliers.

A fresh example of parsing result (.xls file with products information) can be found at `\protected\components\ware\exporter\archive` directory.

Also project comprises few bugs in different places, I intentionally leave they, because this project had no requirements for code and logic quality (in fact, customer wanted only .xls file, and he did not care how).  

After reviewing the code maybe You think: "You don't know about [phpQuery](https://code.google.com/archive/p/phpquery/) library?". Of course I know. But it like a fabric [BMW G11](https://en.wikipedia.org/wiki/BMW_7_Series_(G11)#740e/740Le_iPerformance) and handmade [Ferrari 812 Superfast](https://en.wikipedia.org/wiki/Ferrari_812_Superfast#Performance). Both is good, but last is more *sophisitcated*, *powerful* and *reliable*. So if more concretely this custom parsing engine gives ability to use minimal memory space and increases parsing speed at least in *10 times*.  
Of course making such *Ferraris* takes more time, but on other hand, brings amazing benefits and deep internal satisfaction to its owners.  
Naturally, all of that affects its price, but usually for the fans of "fast driving" it has no matter, because in most cases they are owners of high-loaded systems like Bank APIs, online games, huge shops, etc. The main thing is under the hood, it its engine. The idea of the internal engine brings desired benefits and advancement. Engine is everything.

So, before You a such *Ferrari*.

## Testing

To verify speed of this *parsing engine* or compare with speed of *phpQuery* library to do next steps:  
1) uncomment 14 line in `/index.php` (allow custom debug sets)  
2) uncomment 5, 10 and 11 lines in `/debug/debug.php`  
3) to verify *phpQuery* library parsing speed comment 1158 and uncomment 1164-1215 lines in `/protected/components/ware/Parser.php` and restart *Home* page in brauser, page loading starts parsing process of one saved produt and as result will print total parsing time at top left corner (only one value, something like `0.018000841140747`, description below), update page 5-6 times and You will see average parsing time.  
or  
3) to verify this *parsing engine* parsing speed comment 1158 and uncomment lines 1221-1224 and 1299-1302 in `/protected/components/ware/Parser.php` and do same things as in 3) item above.

A result is saved into variable `$total` and output in page. It is a float number like `0.018000841140747`. First three digits after dot (`018`) is count of milliseconds, and first six digits after dot (`018000`) is count of microseconds. So this particular parsing process with *phpQuery* took `18 milliseconds` or `18 000 microseconds`.  
To have understanding of whole thing, is useful to know that in most cases if web page script is executed in `200 ms` (or less) it is good.

**WARNING:** after testing You **must comment** uncommented lines in `index.php` and `debug.php` files, else when page will be updated, action `AddWares` is started, instead of rendering of *Home* page.

## Screenshots

Is able to see a few screenshots. They are placed at Google Fotos, just visit [project album](https://photos.app.goo.gl/xJhQgVtCEuTLGjd73) and run slideshow (at top right corner, vertical three dots, *Slideshow* menu item).

## Few thoughts

Maybe You seem, that own parser engine is uncomfortable to use. And then You can say that using of cloud tehnologies is solution for every daily a speed and memory problem. Yes, in fact, today it is really most used technique - to install everything (with any time execution and memory consuming) and run new VMs if loading is increased.  
Well...  
At ***first***, such solutions in any case will not be have very low cost. At ***second***, it's security question: if vendor by acident (or not...) leave door/mistake in product, You will have a door in Your system security (a such life), and count of releasing updates says it is real problem. And at ***third***, this approach has one match more big problem...  a such attitude leads a man to technical debility and atrophy of creative ability (that in the aftermath will have an extremely negative impact on his life, or on one of the important spheres of his life). While working on a new idea and a creative process always (in 100% cases) raise a person to a new level (because a practice shows that no stupid men, but exist people, which do nothing in order to become more intelligent).  
So, if You have no time and need to get quickly solution, then open libraries (like *phpQuery*) is ideal variant ( and I self very often go this way :-) ), in other hand, if You have some time and technical skills (or finance ability to buy them), You always can get something better or much more better.

## Technologies

Yii Framework 1.1.17  
PHPExcel 1.8.0  
phpQuery 0.9.5  
jQuery 1.12.4  
Bootstrap 3.3.6  
MySQL 5.7