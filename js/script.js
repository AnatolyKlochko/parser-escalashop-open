( function() {
"use strict"

/**   Globals   */
var finalAttr = 'finish'
var statsPeriodNEW = 2000
var statsPeriodADD = 1000
var statsPeriodDOWNLOAD = 500
var statsContainers = ['statsActionContainer', 'statsLookupsContainer', 'statsNewContainer', 'statsAddedContainer', 'statsUpdatedContainer', 'stop']



/**   Functions   */
/**   transport   */
function ajax(uri, options) {
    $.ajax(uri, options)
}
function post(uri, data, success, error, complete) {
    ajax( uri, 
        {
            method: 'POST',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: success,
            error: error,
            complete: complete
        }
    )
}
function get(uri, data, success, error, complete) {
    ajax( uri, 
        {
            method: 'GET',
            data: data,
            cache: false,
            contentType: false,
            processData: true, // handles 'data' standart way (par1=val1&par2=val2&...)
            dataType: 'json',
            success: success,
            error: error,
            complete: complete
        }
    )
}
function getInputAsFormDataObj($parent, attrArr, defaultAttr) {
    let fd = new FormData, attrObj, attr, attrvalue, key, $domElem, input;
  
    for (let ai = 0, len = attrArr.length; ai < len; ai++) {
        attrObj = attrArr[ai]
        
        if (typeof(attrObj) == 'object') {
            // Object
            attr = attrObj.hasOwnProperty('attrName') ? attrObj.attrName : defaultAttr
            attrvalue = attrObj.attrValue
            key = attrObj.hasOwnProperty('key') ? attrObj.key : attrvalue
        } else {
            // Defaults
            attr = defaultAttr
            attrvalue = attrObj
            key = attrvalue
        }

        // Add to FormData
        if (attrObj.hasOwnProperty('useRaw') && attrObj.useRaw) {
            fd.append(key, attrObj.value)
        } else {
            // Seek DOM Element
            $domElem = $parent.find('['+attr+'^="'+attrvalue+'"]')
            if ($domElem.length > 0) {
                input = $domElem.val()
                if (! input) input = attrObj.defaultValue
                if (input) fd.append(key, input)
            }
        }
    }
    
    return fd
}
/**   UI Helpers   */
var showClass = 'show'
var hideClass = 'hidden'
function cssClassSwitcher(idArr, parent, adding, removing){
    let count = 0;
    parent = (parent) ? `#${parent} ` : ''
    
    // Search for 'id' attr
    idArr.forEach(function(id) {
        let $elem = $( `${parent}#${id}` )
        
        if ($elem.length > 0) {
            // Remove 'hidden'
            if ($elem.hasClass( removing )) $elem.removeClass(removing)
            
            // Add 'show'
            $elem.addClass(adding)
            
            // Increase counter
            count++
        }
    });
    
    // Search for 'class' attr
    if (idArr.length !== count) {
        let $elem = $( `${parent}.${id}` )
        
        if ($elem.length > 0) {
            // Remove 'hidden'
            if ($elem.hasClass( removing )) $elem.removeClass(removing)
            
            // Add 'show'
            $elem.addClass(adding)
        }
    }
}
function showElem(idArr, parent) {
    cssClassSwitcher(idArr, parent, showClass, hideClass)
}
function hideElem(idArr, parent) {
    cssClassSwitcher(idArr, parent, hideClass, showClass)
}


/**   Events   */
$('#config #btnInstallDB').click(function(){
    // Save 'this' to '_this'
    let _this = this
    
    let isBusy = ($(this).attr('isbusy') == 'true')
    if (isBusy) {
        return
    } else {
        $(this).attr('isbusy', 'true')
    }
    
    
    // Variables
    let $stats = $('#config #statsConfig')
    $stats.text('Schema is installing...')
    
    // Request Params
    
    // Request
    let uri = '/site/ConfigInstall'
    post(uri, null, success, null, complete)

    // Response: Success Handler
    function success(response) {
        if (! response.result) {
            // Report
            console.log('Somethings wrong...')
            
            // Stop execution
            throw new Error(response.message)
        } else {
            // Show result
            let m = response.message + '<br />' + response.data.tables + ' tables total.'
            $stats.html(m)
            
            // Hide button
            $(_this).hide()
            
            // Report
            console.log(m)
        }
    }
    
    function complete() {
        // Make accessible
        $(_this).attr('isbusy', 'false')
        
        // Report
        console.log('Process of the Schema Installation was done.')
    }
    
    
    
    // Statistics
    let stats = function s() {
        get('/site/getStatistics?action=install', null, function(response){
            $stats.text(response.message)
            if (! response.hasOwnProperty(finalAttr)) {
                setTimeout(s, 500)
            }
        })
    }
    stats()
})
$('#index #addNewSupplierWares').click(function() {
    // Save 'this' to '_this'
    let _this = this
    
    
    // Lock for use
    let isBusy = ($(this).attr('isbusy') == 'true')
    if (isBusy) {
        return
    } else {
        $(this).attr('isbusy', 'true')
    }
    
    
    // Variables
    let $statsAction = $('#index #statsAction')
    let $statsLookups = $('#index #statsLookups')
    let $statsNew = $('#index #statsNew')
    let $stop = $('#index #stop');
    
    
    // Show Containers
    hideElem(statsContainers, 'index')
    showElem(['statsActionContainer', 'statsLookupsContainer', 'statsNewContainer', 'stop'], 'index')
    // First caption
    $statsAction.text('Initializing...')
    // Stop
    $stop.attr('action', 'new')
    
    
    // Request Params
    let fdParams = [
        { key: 'supplier_id', value: $('#index #suppliers').val() || 1, useRaw: true }
    ]
    let data = getInputAsFormDataObj(null, fdParams, null)
    // Request
    let uri = '/site/AddNewSupplierWares'
    post(uri, data, success, null, complete)


    // Response: Success Handler
    function success(response) {
        if (! response.result) {
            // Report
            console.log('Success: parsing is done.')
            // Stop execution
            throw new Error(response.message)
        }
    }
    function complete() {
        // Make accessible
        $(_this).attr('isbusy', 'false')
        // Stop
        $stop.attr('action', '')
        // Report
        console.log('Complete: parsing process was done.')
    }
    
    
    
    // Statistics
    let last = 0
    let stats = function s() {
        get('/site/getStatistics?action=new', null, function(response){
            // Set data
            if ('message' in response) {
                $statsAction.text(response.message)
                $statsLookups.text(response.data.lookups)
                $statsNew.text(response.data.new)
            }
            
            // Verify finish
            if (! $stop.attr('action')) {
                if (last === 0) last = Date.now()
                if (Date.now() - last > 5000) {
                    return
                }
            }
            if (! response.hasOwnProperty(finalAttr)) {
                setTimeout(s, statsPeriodNEW)
            }
        }, function(error){
            //console.log('Error: ')
            //console.log(error)
        }, function(response){
            //console.log('Complete: ')
            //console.log(response)
        })
    }
        
    // Request Params
    let fdParamsStats = [
        { key: 'action', value: 'new', useRaw: true }
    ]
    let dataStats = getInputAsFormDataObj(null, fdParamsStats, null)
    // Clear previous stats
    post('/site/clearStatistics', dataStats, successClerStats)
    
    function successClerStats(response) {
        if (response.result) {
            stats()
        }
    }
    
})
$('#index #addNewWares').click(function() {
    // Save 'this' to '_this'
    let _this = this
    
    
    // Lock for use
    let isBusy = ($(this).attr('isbusy') == 'true')
    if (isBusy) {
        return
    } else {
        $(this).attr('isbusy', 'true')
    }
    
    
    // Variables
    let $statsAction = $('#index #statsAction')
    let $statsAdded = $('#index #statsAdded')
    let $stop = $('#index #stop');
    
    
    
    // Show Containers
    hideElem(statsContainers, 'index')
    showElem(['statsActionContainer', 'statsAddedContainer', 'stop'], 'index')
    // First caption
    $statsAction.text('Initializing...')
    // Stop
    $stop.attr('action', 'add')
    
    
    // Request Params
    let fdParams = [
        { key: 'supplier_id', value: $('#index #suppliers').val() || 1, useRaw: true }
    ]
    let data = getInputAsFormDataObj(null, fdParams, null)
    // Request
    let uri = '/site/AddNewWares'
    post(uri, data, success, null, complete)


    // Response: Success Handler
    function success(response) {
        if (! response.result) {
            // Report
            console.log('Success: parsing is done.')
            // Stop execution
            throw new Error(response.message)
        }
    }
    function complete() {
        // Make accessible
        $(_this).attr('isbusy', 'false')
        // Stop
        $stop.attr('action', '')
        // Report
        console.log('Complete: parsing process was done.')
    }
    
    
    
    // Statistics
    let last = 0
    let stats = function s() {
        get('/site/getStatistics?action=add', null, function(response){
            // Set data
            if ('message' in response) {
                $statsAction.text(response.message)
                $statsAdded.text(response.data.added)
            }
            
            // Verify finish
            if (! $stop.attr('action')) {
                if (last === 0) last = Date.now()
                if (Date.now() - last > 5000) {
                    return
                }
            }
            if (! response.hasOwnProperty(finalAttr)) {
                setTimeout(s, statsPeriodADD)
            }
        }, function(error){
            //console.log('Error: ')
            //console.log(error)
        }, function(response){
            //console.log('Complete: ')
            //console.log(response)
        })
    }
        
    // Request Params
    let fdParamsStats = [
        { key: 'action', value: 'add', useRaw: true }
    ]
    let dataStats = getInputAsFormDataObj(null, fdParamsStats, null)
    // Clear previous stats
    post('/site/clearStatistics', dataStats, successClerStats)
    
    function successClerStats(response) {
        if (response.result) {
            stats()
        }
    }
    
})
$('#index #downloadXLS').click(function(){
    // Variables
    let $statsAction = $('#index #statsAction')
        
    // Show Containers
    hideElem(statsContainers, 'index')
    showElem(['statsActionContainer'], 'index')
    // First caption
    $statsAction.text('Making new archive...')
    
    
    
    // Download init
    $('#downloadXLSForm').submit();
    
    
    
    // Statistics
    let last = 0
    let stats = function s() {
        get('/site/getStatistics?action=download', null, function(response){
            // Set data
            if ('message' in response) {
                $statsAction.text(response.message)
            }
            
            // Verify finish
            if (! response.hasOwnProperty(finalAttr)) {
                setTimeout(s, statsPeriodDOWNLOAD)
            }
        }, function(error){
            //console.log('Error: ')
            //console.log(error)
        }, function(response){
            //console.log('Complete: ')
            //console.log(response)
        })
    }
        
    // Request Params
    let fdParamsStats = [
        { key: 'action', value: 'download', useRaw: true }
    ]
    let dataStats = getInputAsFormDataObj(null, fdParamsStats, null)
    // Clear previous stats
    post('/site/clearStatistics', dataStats, successClerStats)
    
    function successClerStats(response) {
        if (response.result) {
            stats()
        }
    }
})
$('#index #stop').click(function() {
    // Save 'this' to '_this'
    let _this = this
    let action = $(this).attr('action')
    
    // Lock for use
    let isBusy = ($(this).attr('isbusy') == 'true')
    if (isBusy || !action) {
        return
    } else {
        $(this).attr('isbusy', 'true')
    }
    
    
    // Request Params
    let fdParams = [
        { key: 'action', value: action, useRaw: true }
    ]
    let data = getInputAsFormDataObj(null, fdParams, null)
    // Request
    let uri = '/site/stop'
    post(uri, data, success, null, complete)


    // Response: Success Handler
    function success(response) {
        if (! response.result) {
            // Report
            console.log('Action \''+action+'\' is stopped.')
            // Stop execution
            throw new Error(response.message)
        } else {
            $('#index #statsAction').text('Stopped')
        }
    }
    function complete() {
        // Make accessible
        $(_this).attr('isbusy', 'false')
        // Clear
        $(_this).removeAttr('action')
    }
})

})()