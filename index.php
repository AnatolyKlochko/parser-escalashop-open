<?php

/**
 * If autoloaders connect after Yii autoloader, they will never call (because Yii
 * autoloader after search, if class was not found, throws exception and stops
 * executing).
 */
require __DIR__.'/protected/components/ware/Autoloader.php'; // al for  Ware  classes - SupplierBase, Supplier1, etc.
require __DIR__.'/protected/vendor/PHPExcel/1.8.0/PHPExcel.php'; // al for  PHPExcel library



// D E B U G
//require './debug/debug.php';



//   YII  framework

// change the following paths if necessary
$yii=dirname(__FILE__).'/framework/yii.php';
$config=dirname(__FILE__).'/protected/config/main.php';

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
Yii::createWebApplication($config)->run();
