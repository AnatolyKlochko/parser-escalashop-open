<?php

// WARNING: it will load ALL files in these directories. If it will big, it will very BIG "PLUS" for speed.
//Yii::import('application.components.ware.*');
//Yii::import('application.components.ware.supplier.*');


class SiteController extends Controller
{
       
    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.♂
     */
    public function actionIndex()
    {
        $this->render('index');
    }

    public function actionData() 
    {
        $this->render('data');
    }
    
    public function actionConfig()
    {
        $this->render('config');
    }
    
    public function actionConfigInstall()
    {
        // Statistics
        $action = 'install';
        $data = [
            'start' => microtime(true),
            'message' => 'Get connection....'
        ];
        Statistics::setData($action, $data);
        sleep(1);
        
        // DB connection
        $mysql_cnf = Schema::getDbConfig( Schema::getDbConfigPath() );
        $mysql = Schema::getDbh($mysql_cnf);
        
        
        
        // Statistics
        $data['message'] = 'Execute script....';
        Statistics::setData($action, $data);
        sleep(1);
        
        
        
        // Get script
        global $config;
        $path = dirname($config) . '/../data/escala.sql';
        $sql = file_get_contents($path);
        
        // Run script
        $result = $mysql->multi_query($sql);
        //$mysql->close();
                
        // Send response
        if ($result) {
            // Tables count
            sleep(10); // wait for `Information_Schema` updating (overwise count of tables will equal 1)
            $mysql2 = Schema::getDbh($mysql_cnf);
            $tables = Schema::getTablesCount($mysql2, $mysql_cnf['db']);
            //$mysql2->close();
            
            $data['message'] = 'Database installed successfully.';
            $data['data'] = [
                'tables' => &$tables
            ];
            $data['result'] = true;
        } else {
            $data['message'] = 'Error of Database installing. MySQL Error: ' . $mysql->error;
            $data['result'] = false;
        }
        

        
        // Statistics
        $data['finish'] = microtime(true);
        Statistics::setData($action, $data);
        sleep(1);
        
        
        
        // Response
        echo json_encode($data, JSON_PRETTY_PRINT);
    }
    
    /**
     * This is the action to handle external exceptions.
     */
    public function actionError()
    {
        if ($error=Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
    
    /**
     * Finds new products in a supplier site.
    */
    public function actionAddNewSupplierWares()
    {
        // Check Supplier ID    
        if (isset($_POST['supplier_id'])) {
            $supplier_id = (int)$_POST['supplier_id'];
        } else {
            echo 'FATAL ERROR: no Supplier ID.';
            exit();
        } 
        
        
        // Off stop
        Statistics::stopOff('new');
        
        
        // Create new Supplier object
        $supplier = 'Supplier_' . $supplier_id;
        $supplier = new $supplier();
        
        // start scanning 
        $supplier->AddNewSupplierWares();
        
    }
    
    /**
     * Adds found products to Database. Uses links from `SupplierWare` table to
     * download supplier products pages (and parse they then).
     */
    public function actionAddNewWares()
    {
        // Check Supplier ID    
        if(isset($_POST['supplier_id'])){
            $supplier_id = &$_POST['supplier_id'];
        } else {
            echo 'FATAL ERROR: no Supplier ID.';
            exit();
        } 
         
        
        
        // Off stop
        Statistics::stopOff('add');
        
        
        
        // Create new Supplier object
        $supplier = 'Supplier_' . $supplier_id;
        $supplier = new $supplier();
        
        // add found wares to OUR DB
        $supplier->AddNewWares();
        
    }
    
    /**
     * Updates existing products.
    */
    public function actionUpdateWares()
    {
        
    }
    
    public function actionClearStatistics()
    {
        // Check Supplier ID    
        if(isset($_POST['action'])){
            $action = $_POST['action'];
        } else {
            echo json_encode(['result' => false]);
            exit();
        }
        
        Statistics::clear($action);
        
        echo json_encode(['result' => true]);
    }
    
    public function actionStop()
    {
        // Check Supplier ID    
        if(isset($_POST['action'])){
            $action = $_POST['action'];
        } else {
            echo json_encode(['result' => false, 'message' => 'FATAL ERROR: no action name.']);
            exit();
        }
        
        Statistics::stopAction($action);
        
        echo json_encode(['result' => true]);
    }
    
    public function actionExportToXLS()
    {
        $exp = new Exporter();
        $file = $exp->ExportWaresAndCategoriesToXLS();
        return $file;
    }
    
    public function actionDownloadXLS()
    {
        // Statistics
        $statsInfo = [
            'result' => true,
            'message' => 'Making new archive...',
            'start' => microtime(true)
        ];
        Statistics::setData('download', $statsInfo);
        
        
        
        // Make new .xls
        $file = $this->actionExportToXLS();
        
        if (file_exists($file)) {
            // Statistics
            $statsInfo['message'] = 'Prepare downloading...';
            Statistics::setData('download', $statsInfo);
            sleep(2);



            // Download new hot file
            header('Content-Description: File Transfer');
            header('Content-Type: application/vnd.ms-excel');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"" . basename($file) . "\"");
            readfile($file);



            // Statistics
            $statsInfo['message'] = 'Preparing is finished (wait for file save dialog).';
            $statsInfo['finish'] = microtime(true);
            Statistics::setData('download', $statsInfo);
        } else {
            // Statistics
            $statsInfo['message'] = 'Archive making fail. No file to download.';
            $statsInfo['finish'] = microtime(true);
            Statistics::setData('download', $statsInfo);
        }
        
    }
    
    /**
     * Gives needly information about executing parsing process.
     */
    public function actionGetStatistics()
    {
        $action = &$_GET['action'];
        $dataJSON = Statistics::getData($action);
        if ($dataJSON) {
            echo $dataJSON;
        } else {
            echo json_encode(['result' => true]);
        }
    }
    
    
    /** H E L P E R S  */
    
    
    
    public function actionTest()
    {
        exit;
        
        
        // MySQL Server connection
        $mysqli = new mysqli("localhost", "root", "", "Escala");
        if($mysqli === FALSE) {
            echo 'Erise error on MySQL Server side:'.mysqli_error;
            exit();
        }
		
        // SQL string, table Category
        $sql =  'SELECT CategoryID, CategoryIDInPROMUA, Name, ParentCategoryID, PROMUACategoryID '. 
                'FROM Category '.
                //'ORDER BY ParentCategoryID, Name '.
                //'LIMIT 10 '.
                '';
        $sql .= ';';
        
        // Get rows
        $result = $mysqli->query("SET NAMES UTF8;");  
        $category_table = $mysqli->query($sql);
        $category_table = $category_table->fetch_all(MYSQLI_ASSOC);       
    }
    public function actionTestForm()
    {
        if (! empty($_POST['my_path'])) {
            $dir = $_POST['my_path'];//dirname(__FILE__);
            //$dir_and_files = scandir($dir, SCANDIR_SORT_NONE);
        }
        
    }
}
