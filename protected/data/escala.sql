-- phpMyAdmin SQL Dump
-- version 4.7.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: May 08, 2018 at 10:15 PM
-- Server version: 5.7.13
-- PHP Version: 7.1.1

SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `escala_test2`
--

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE IF NOT EXISTS `category` (
  `CategoryID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(200) NOT NULL,
  `ParentCategoryID` smallint(5) UNSIGNED DEFAULT '0',
  `CategoryIDInPROMUA` int(10) UNSIGNED NOT NULL,
  `PROMUACategoryID` int(10) UNSIGNED DEFAULT NULL,
  `PROMUACategoryLINK` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`CategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(1, 'Корневая группа', 0, 9936952, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(2, 'Одежда женская', 69, 9956614, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(3, 'Платья', 2, 9995450, NULL, 'http://prom.ua/Zhenskii-platya');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(4, 'Пальта', 2, 10226379, NULL, 'http://prom.ua/Palto');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(5, 'Гольфы, кофты, рубашки', 2, 10227823, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(6, 'Костюмы деловые', 2, 10229109, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(7, 'Брюки', 2, 10232996, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(8, 'Пиджаки, жилеты, кардиганы', 2, 10240771, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(9, 'Юбки', 2, 10259505, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(10, 'Мягкие игрушки', 0, 10319181, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(11, 'Игрушки Мишки', 10, 10319182, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(12, 'Игрушки подушки', 10, 10319183, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(13, 'Игрушки Собачки', 10, 10319184, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(14, 'Игрушки Зайцы', 10, 10319185, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(15, 'Игрушки из м/ф', 10, 10319186, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(16, 'Игрушки Слоники', 10, 10319188, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(17, 'Игрушки Панды', 10, 10319189, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(18, 'Игрушки Обезьянки', 10, 10319190, NULL, 'http://prom.ua/Myagkie-plyushevye-igrushki');
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(19, 'Обувь', 0, 10795108, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(20, 'Туфли', 19, 10795967, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(21, 'Ботинки', 19, 10795975, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(22, 'Кроссовки', 19, 10795978, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(23, 'Берцы', 19, 10795992, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(24, 'Тапочки', 19, 10796006, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(25, 'Мокасины слипоны', 19, 10796031, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(26, 'Сандали', 19, 10796033, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(27, 'Туники', 2, 10893186, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(28, 'Свитера', 2, 10893302, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(29, 'Куртки', 2, 10912091, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(30, 'Шубы женские', 2, 10912272, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(31, 'Лосины', 2, 10912355, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(32, 'Новинки', 0, 12406207, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(33, 'Обувь женская', 19, 12406169, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(34, 'Колготки', 2, 12426637, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(35, 'Майки, футболки, топы', 2, 12426648, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(36, 'Аксессуары', 2, 12426668, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(37, 'Купальники', 2, 12426740, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(38, 'Костюмы, комбинезоны', 2, 12427474, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(39, 'Сумки', 0, 12525837, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(40, 'Сумки женские', 39, 12525846, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(41, 'Блузы', 2, 12525838, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(42, 'Шорты', 2, 12525843, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(43, 'Головные уборы, шарфы', 2, 12525839, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(44, 'Новинки', 2, 12525841, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(45, 'Платья в пол', 3, 12525844, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(46, 'Дубленки', 2, 12525840, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(47, 'Пуховики', 2, 12525842, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(48, 'Куртки зимние', 29, 12525845, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(49, 'Одежда мужская', 69, 12563354, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(50, 'Рубашки', 49, 12563370, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(51, 'Костюмы', 49, 12563373, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(52, 'Пиджаки', 49, 12563378, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(53, 'Брюки', 49, 12563386, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(54, 'Куртки', 49, 12563395, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(55, 'Пальто', 49, 12563401, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(56, 'Кофты', 49, 12563410, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(57, 'Футболки', 49, 12563414, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(58, 'Галстуки', 49, 12563434, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(59, 'Бабочки', 49, 12563449, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(60, 'Ремни', 49, 12563453, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(61, 'Шарфы', 49, 12563459, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(62, 'Моншер', 49, 12563464, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(63, 'Свадебные товары', 49, 12563466, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(64, 'Костюмы', 63, 12563470, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(65, 'Жилеты', 63, 12563474, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(66, 'Пояс + бабочка + платок', 63, 12563478, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(67, 'Пластрон', 63, 12563482, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(68, 'Обувь мужская', 19, 12564757, NULL, NULL);
INSERT INTO `category` (`CategoryID`, `Name`, `ParentCategoryID`, `CategoryIDInPROMUA`, `PROMUACategoryID`, `PROMUACategoryLINK`) VALUES(69, 'Одежда', 0, 12564733, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categorymap`
--

DROP TABLE IF EXISTS `categorymap`;
CREATE TABLE IF NOT EXISTS `categorymap` (
  `CategoryMapID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierCategoryID` smallint(5) UNSIGNED NOT NULL,
  `CategoryID` smallint(5) UNSIGNED NOT NULL,
  `ConformityMayBeChanged` bit(1) DEFAULT b'0',
  PRIMARY KEY (`CategoryMapID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categorymap`
--

INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(1, 1, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(2, 2, 19, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(3, 3, 32, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(4, 4, 3, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(5, 5, 31, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(6, 6, 28, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(7, 7, 5, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(8, 8, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(9, 9, 5, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(10, 10, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(11, 11, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(12, 12, 6, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(13, 13, 8, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(14, 14, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(15, 15, 7, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(16, 16, 9, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(17, 17, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(18, 18, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(19, 19, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(20, 20, 21, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(21, 21, 19, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(22, 22, 20, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(23, 23, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(24, 24, 32, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(25, 25, 5, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(26, 26, 3, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(27, 27, 8, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(28, 28, 34, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(29, 29, 35, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(30, 30, 7, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(31, 31, 28, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(32, 32, 38, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(33, 33, 9, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(34, 34, 36, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(35, 35, 4, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(36, 36, 37, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(37, 37, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(38, 38, 3, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(39, 39, 28, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(40, 40, 40, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(41, 41, 41, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(42, 42, 38, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(43, 43, 35, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(44, 44, 42, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(45, 45, 31, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(46, 46, 8, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(47, 47, 37, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(48, 48, 38, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(49, 49, 38, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(50, 50, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(51, 51, 43, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(52, 52, 9, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(53, 54, 44, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(54, 55, 6, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(55, 56, 45, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(56, 57, 35, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(57, 58, 31, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(58, 59, 29, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(59, 60, 46, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(60, 61, 47, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(61, 62, 8, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(62, 63, 4, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(63, 64, 48, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(64, 53, 2, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(65, 65, 49, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(66, 66, 50, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(67, 67, 50, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(68, 68, 50, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(69, 69, 50, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(70, 70, 50, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(71, 71, 51, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(72, 72, 51, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(73, 73, 51, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(74, 74, 51, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(75, 75, 51, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(76, 76, 52, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(77, 77, 52, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(78, 78, 52, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(79, 79, 53, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(80, 80, 53, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(81, 81, 53, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(82, 82, 53, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(83, 83, 49, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(84, 84, 54, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(85, 85, 55, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(86, 86, 49, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(87, 87, 56, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(88, 88, 57, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(89, 89, 49, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(90, 90, 58, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(91, 91, 59, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(92, 92, 60, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(93, 93, 61, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(94, 94, 61, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(95, 95, 62, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(96, 96, 63, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(97, 97, 64, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(98, 98, 65, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(99, 99, 66, b'1');
INSERT INTO `categorymap` (`CategoryMapID`, `SupplierCategoryID`, `CategoryID`, `ConformityMayBeChanged`) VALUES(100, 100, 67, b'1');

-- --------------------------------------------------------

--
-- Table structure for table `parsinginfo`
--

DROP TABLE IF EXISTS `parsinginfo`;
CREATE TABLE IF NOT EXISTS `parsinginfo` (
  `ParsingInfoID` smallint(5) UNSIGNED NOT NULL,
  `ParsingDATE` date NOT NULL,
  `ParsingStartTIME` time NOT NULL,
  `ParsingEndTIME` time NOT NULL,
  `TotalWares` mediumint(9) NOT NULL,
  PRIMARY KEY (`ParsingInfoID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pattern`
--

DROP TABLE IF EXISTS `pattern`;
CREATE TABLE IF NOT EXISTS `pattern` (
  `PatternID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierID` smallint(5) UNSIGNED NOT NULL,
  `SupplierCategoryID` smallint(5) UNSIGNED DEFAULT NULL,
  `ParentPatternID` smallint(5) UNSIGNED DEFAULT NULL,
  `PatternTypeID` tinyint(3) UNSIGNED NOT NULL,
  `WareFeatureID` tinyint(3) UNSIGNED DEFAULT NULL,
  `ResultSingle` bit(1) NOT NULL DEFAULT b'1',
  `Text` tinytext NOT NULL,
  PRIMARY KEY (`PatternID`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pattern`
--

INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(1, 1, NULL, NULL, 1, NULL, b'1', '<header>.*<div\\sclass=\"center\">.*<ul\\sclass=\"nav\">(.+)<\\/header>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(2, 1, NULL, 1, 2, NULL, b'1', '<li>.+href=\"(?<link>.+)\".+>(?<name>.+)<.+<\\/li>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(3, 1, NULL, NULL, 3, NULL, b'1', '<\\/header>.*<div\\sclass=\"sidebar\".*<ul.+>(.+)<\\/ul>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(4, 1, NULL, 3, 4, NULL, b'1', '<li>.+href=\"(?<link>.+)\".+>(?<name>.+)<.+<\\/li>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(5, 1, NULL, NULL, 6, NULL, b'1', '<div\\sclass=\"js_catalog\">(.+)<div\\sclass=\"category_m\">');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(7, 1, NULL, 5, 7, NULL, b'1', '<div\\sclass=\"items_new_product\\scategory_itms\">.*<a\\shref=\"(?<link>.+)\".+>.*<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(8, 1, NULL, NULL, 8, NULL, b'1', '<div\\s.*class=\"product\">(.+)<div\\sclass=\"popular_product\">');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(9, 1, NULL, 8, 9, 1, b'1', '<div\\sclass=\"pic\">.*<img\\s.*src=\"(?<ImageFace>.+)\".*>.*<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(10, 1, NULL, 8, 9, 2, b'1', '<li>.*<img.*data-big=\"(?<Image>.+)\".*>.*<\\/li>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(12, 1, NULL, 8, 9, 3, b'1', '<div\\sitemprop=\"name\".*>(?<Title>[^\\s\\d\\/]+)\\s.*<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(13, 1, NULL, 8, 9, 5, b'1', '<div\\sitemprop=\"description\".*>(?<Description>.+)<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(14, 1, NULL, 8, 9, 4, b'1', '<span\\sitemprop=\"price\"><.*>(?<Price>.+)<\\/span>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(15, 1, NULL, 8, 9, 6, b'1', '<span\\sitemprop=\"price\"><meta.+content=\"(?<Valuta>.+)\">.*<\\/span>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(16, 1, NULL, 8, 9, 7, b'1', '<select\\sname=\"size\".+>(?<Size>.+)<\\/select>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(18, 1, NULL, 8, 9, 9, b'1', '<div\\sitemprop=\"description\".*>.*<p>(?<Title2>.+)<.*<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(19, 1, NULL, 8, 9, 10, b'1', '<div\\sclass=\"status\">.*<div\\sclass=\"in_stock\">(?<Availability>.+)<\\/div>.*<\\/div>');
INSERT INTO `pattern` (`PatternID`, `SupplierID`, `SupplierCategoryID`, `ParentPatternID`, `PatternTypeID`, `WareFeatureID`, `ResultSingle`, `Text`) VALUES(20, 1, NULL, 8, 9, 11, b'1', '<div\\sclass=\"articul\">(?<Articul>.+)<\\/div>');

-- --------------------------------------------------------

--
-- Table structure for table `patterntype`
--

DROP TABLE IF EXISTS `patterntype`;
CREATE TABLE IF NOT EXISTS `patterntype` (
  `PatternTypeID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(30) DEFAULT NULL,
  `Description` text,
  PRIMARY KEY (`PatternTypeID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `patterntype`
--

INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(1, 'top_category_block', 'PARENT for TOP_CATEGORY. NOTE: BLOCK can not have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(2, 'top_category', 'NOTE: subpattern MUST have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(3, 'category_block', 'PARENT for CATEGORY. NOTE: BLOCK can not have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(4, 'category', 'NOTE: subpattern MUST have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(5, 'wares_list_link', NULL);
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(6, 'wares_block', 'PARENT for WARE_LINK. NOTE: BLOCK can not have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(7, 'ware_link', 'NOTE: subpattern MUST have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(8, 'ware_block', 'PARENT for WARE_FEATURE. NOTE: BLOCK can not have PARENT');
INSERT INTO `patterntype` (`PatternTypeID`, `Name`, `Description`) VALUES(9, 'ware_feature', 'NOTE: subpattern MUST have PARENT');

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `SupplierID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Description` tinytext,
  `SiteLINK` varchar(255) NOT NULL,
  `TopCategoryListLINK` varchar(255) NOT NULL,
  `MarkUp` smallint(6) DEFAULT '100',
  `CharacterSet` varchar(30) DEFAULT 'UTF-8',
  `NewWareParsingStartDATETIME` datetime DEFAULT '0000-00-00 00:00:00',
  `NewWareParsingEndDATETIME` datetime DEFAULT '0000-00-00 00:00:00',
  `NewWares` smallint(6) UNSIGNED DEFAULT '0',
  `TotalWares` smallint(6) UNSIGNED DEFAULT NULL,
  PRIMARY KEY (`SupplierID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`SupplierID`, `Name`, `Description`, `SiteLINK`, `TopCategoryListLINK`, `MarkUp`, `CharacterSet`, `NewWareParsingStartDATETIME`, `NewWareParsingEndDATETIME`, `NewWares`, `TotalWares`) VALUES(1, '53Mission', 'Женская одежда', 'http://53mission.com/', 'http://53mission.com/', 100, 'DEFAULT', '2016-06-22 00:00:00', '2016-06-22 00:00:00', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `suppliercategory`
--

DROP TABLE IF EXISTS `suppliercategory`;
CREATE TABLE IF NOT EXISTS `suppliercategory` (
  `SupplierCategoryID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierID` smallint(5) UNSIGNED NOT NULL,
  `ParentCategoryID` smallint(5) UNSIGNED NOT NULL DEFAULT '0' COMMENT '0 - is TOP Category',
  `Name` varchar(255) NOT NULL,
  `LINK` varchar(255) NOT NULL,
  `TotalWaresCount` mediumint(8) UNSIGNED DEFAULT NULL,
  `Exist` bit(1) DEFAULT b'1',
  `Hidden` bit(1) DEFAULT b'0',
  `NotCategory` bit(1) DEFAULT b'0',
  `MultiCategory` bit(1) DEFAULT b'0',
  PRIMARY KEY (`SupplierCategoryID`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliercategory`
--

INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(1, 1, 0, 'Одежда', 'http://53mission.com/catalog/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(2, 1, 0, 'Обувь', 'http://53mission.com/women-shoes/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(3, 1, 1, 'Новинки', 'http://53mission.com/catalog/novinki/', NULL, b'1', b'1', b'1', b'1');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(4, 1, 1, 'Платья', 'http://53mission.com/catalog/platya/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(5, 1, 1, 'Лосины', 'http://53mission.com/catalog/losinyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(6, 1, 1, 'Свитера', 'http://53mission.com/catalog/svitera/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(7, 1, 1, 'Кофты', 'http://53mission.com/catalog/koftyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(8, 1, 1, 'Комбинезоны', 'http://53mission.com/catalog/kombinezonyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(9, 1, 1, 'Рубашки, блузы', 'http://53mission.com/catalog/rubashki-bluzyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(10, 1, 1, 'Свитшоты', 'http://53mission.com/catalog/svitshotyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(11, 1, 1, 'Сарафаны', 'http://53mission.com/catalog/sarafany/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(12, 1, 1, 'Костюмы', 'http://53mission.com/catalog/kostyumyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(13, 1, 1, 'Кардиганы, пиджаки', 'http://53mission.com/catalog/kardiganyi-pidzhaki/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(14, 1, 1, 'Платья офисные', 'http://53mission.com/catalog/platya-ofisnyie/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(15, 1, 1, 'Брюки, штаны', 'http://53mission.com/catalog/bryuki-shtanyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(16, 1, 1, 'Юбки', 'http://53mission.com/catalog/yubki/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(17, 1, 1, 'Футболки, майки', 'http://53mission.com/catalog/futbolki-majki/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(18, 1, 1, 'Шорты', 'http://53mission.com/catalog/shortyi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(19, 1, 1, 'Верхняя одежда', 'http://53mission.com/catalog/verhnyaya-odezhda/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(20, 1, 2, 'Ботинки', 'http://53mission.com/women-shoes/botinki/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(21, 1, 2, 'Сапоги', 'http://53mission.com/women-shoes/sapogi/', NULL, b'1', b'1', b'1', b'0');
INSERT INTO `suppliercategory` (`SupplierCategoryID`, `SupplierID`, `ParentCategoryID`, `Name`, `LINK`, `TotalWaresCount`, `Exist`, `Hidden`, `NotCategory`, `MultiCategory`) VALUES(22, 1, 2, 'Туфли', 'http://53mission.com/women-shoes/tufli/', NULL, b'1', b'1', b'1', b'0');

-- --------------------------------------------------------

--
-- Table structure for table `supplierware`
--

DROP TABLE IF EXISTS `supplierware`;
CREATE TABLE IF NOT EXISTS `supplierware` (
  `SupplierWareID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierID` smallint(5) UNSIGNED NOT NULL,
  `SupplierCategoryID` smallint(5) UNSIGNED NOT NULL,
  `LINK` varchar(255) NOT NULL,
  `Exist` bit(1) DEFAULT b'1',
  `Hidden` bit(1) DEFAULT b'0',
  `IsNew` bit(1) DEFAULT b'0',
  `ParsePrevTIME` timestamp NULL DEFAULT NULL,
  `ParseTIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SupplierWareID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `supplierwarefeatureprocessing`
--

DROP TABLE IF EXISTS `supplierwarefeatureprocessing`;
CREATE TABLE IF NOT EXISTS `supplierwarefeatureprocessing` (
  `SupplierWareFeatureProcessingID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierID` smallint(5) UNSIGNED NOT NULL,
  `SupplierCategoryID` smallint(5) UNSIGNED DEFAULT NULL,
  `WareFeatureID` tinyint(3) UNSIGNED NOT NULL,
  `Direction` enum('ToDB','FromDB') NOT NULL,
  PRIMARY KEY (`SupplierWareFeatureProcessingID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplierwarefeatureprocessing`
--

INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(1, 1, NULL, 2, 'ToDB');
INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(2, 1, NULL, 5, 'ToDB');
INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(3, 1, NULL, 7, 'ToDB');
INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(4, 1, NULL, 9, 'ToDB');
INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(5, 1, NULL, 10, 'ToDB');
INSERT INTO `supplierwarefeatureprocessing` (`SupplierWareFeatureProcessingID`, `SupplierID`, `SupplierCategoryID`, `WareFeatureID`, `Direction`) VALUES(6, 1, NULL, 11, 'ToDB');

-- --------------------------------------------------------

--
-- Table structure for table `ware`
--

DROP TABLE IF EXISTS `ware`;
CREATE TABLE IF NOT EXISTS `ware` (
  `WareID` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `SupplierID` smallint(5) UNSIGNED NOT NULL,
  `SupplierCategoryID` smallint(5) UNSIGNED NOT NULL,
  `SupplierWareID` int(10) UNSIGNED NOT NULL,
  `Articul` varchar(20) DEFAULT NULL,
  `Code` varchar(15) DEFAULT NULL,
  `Title` varchar(110) NOT NULL,
  `Title2` varchar(110) DEFAULT NULL COMMENT 'Improve (more informative, beauty) title for ware',
  `KeyWords` varchar(255) NOT NULL,
  `Description` varchar(10000) NOT NULL,
  `WareType` char(1) DEFAULT NULL COMMENT 'r - розница, w - оптом, u - и оптом и в розницу, s - услуга',
  `Price` float(10,2) UNSIGNED DEFAULT '0.00',
  `Valuta` varchar(10) DEFAULT 'UAH' COMMENT 'только след. значения: UAH, USD, EUR, RUB, CHF, GBP, JPY, PLZ, BYR, KZT, MDL, другая',
  `MeasureUnit` varchar(10) DEFAULT 'шт.' COMMENT 'только след. значения: шт., т, кг, г, куб. м, л, кв.м, м, км, дал, мешок, пара, упаковка, чел.ед., час, день, неделя, месяц',
  `MinQuantOrder` int(11) DEFAULT '1',
  `WholesalePrice` varchar(60) DEFAULT NULL COMMENT 'должен быть уникальным и соответственным MinWholesaleOrder',
  `MinWholesaleOrder` varchar(60) DEFAULT NULL COMMENT 'должен быть уникальным и соответственным WholesalePrice',
  `ImageFace` varchar(255) DEFAULT NULL COMMENT 'Лицевое фото.',
  `Image` varchar(3000) DEFAULT NULL COMMENT 'Может быть несколько фото. Разделитель: запятая плюс пробел.',
  `Availability` varchar(5) DEFAULT '+' COMMENT '«+» — есть в наличии. «-» — нет в наличии. «@» — услуга. «цифра» (например «9») — товар под заказ, цифра=кол-во дней на доставку. Пустое поле — выводится статус товара "Наличие не известно".',
  `Discount` float(5,2) UNSIGNED DEFAULT NULL COMMENT 'Положительное число или число с %. Если у товара есть скидка, в данном поле указывается величина скидки или процент. Пример: 12.5, 30%.',
  `Manufacturer` varchar(255) DEFAULT NULL,
  `CountryManufacturer` varchar(30) DEFAULT NULL,
  `DeliveryQuantity` smallint(5) UNSIGNED DEFAULT NULL COMMENT 'объем поставки (сколько мы можем поставить). Только для оптовых товаров',
  `DeliveryPeriod` varchar(8) DEFAULT NULL COMMENT 'only next values: день, неделя, месяц, квартал, год',
  `PackagingWay` varchar(500) DEFAULT NULL,
  `WareIdentifier` varchar(10) DEFAULT NULL COMMENT '?length, can contain digits & chars',
  `UnicalIdentifier` int(10) UNSIGNED DEFAULT NULL COMMENT 'создается ПРОМОМ при экспорте',
  `WareGroupID` int(10) UNSIGNED DEFAULT NULL,
  `Size` varchar(300) DEFAULT NULL COMMENT 'см.куб;1500,1700,2000  или  ;X,XL,XXL   или   ;32,33,34,35',
  `Color` varchar(300) DEFAULT NULL COMMENT 'розовый,малиновый,белый',
  `ExistsByLINK` bit(1) DEFAULT b'1',
  `Hidden` bit(1) DEFAULT b'0',
  `CreatedTIME` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`WareID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `warefeature`
--

DROP TABLE IF EXISTS `warefeature`;
CREATE TABLE IF NOT EXISTS `warefeature` (
  `WareFeatureID` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Name` varchar(50) NOT NULL COMMENT 'the each VALUE in this column must IDENTICAL to COLUMN NAME in Ware table',
  PRIMARY KEY (`WareFeatureID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `warefeature`
--

INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(1, 'ImageFace');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(2, 'Image');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(3, 'Title');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(4, 'Price');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(5, 'Description');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(6, 'Valuta');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(7, 'Size');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(8, 'Color');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(9, 'Title2');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(10, 'Availability');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(11, 'Articul');
INSERT INTO `warefeature` (`WareFeatureID`, `Name`) VALUES(12, 'Manufacturer');
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
