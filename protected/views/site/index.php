<div id="index" class="row">
    <div class="col-md-2">
        <select id="suppliers" class="form-control" style="padding-top: 5px !important; padding-bottom: 5px !important;">
            <option selected disabled>Choose Supplier</option>
            <option value="1">53mission.com</option>
        </select>
    </div>
    <div class="col-md-10">
        <div class="">
            <button id="addNewSupplierWares" type="button" class="btn btn-primary">New Products</button>
            <button id="addNewWares" type="button" class="btn btn-primary">Add Products</button>
            <button id="updateWares" type="button" class="btn btn-primary">Update Products</button>
            <button id="downloadXLS" type="button" class="btn btn-primary">Download XLS</button>
            <form class="hidden" id="downloadXLSForm" action="/site/downloadXLS"></form>
        </div>
        
        <div class="row" id="statistics"></div>
        <div class="row">
            <div class="col-md-5">
                <div class="row hidden" id="statsActionContainer">
                    <div class="col-md-6">Action: </div>
                    <div class="col-md-6" id="statsAction"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row hidden" id="statsLookupsContainer">
                    <div class="col-md-6">Lookups: </div>
                    <div class="col-md-6" id="statsLookups"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row hidden" id="statsNewContainer">
                    <div class="col-md-6">New: </div>
                    <div class="col-md-6" id="statsNew"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row hidden" id="statsAddedContainer">
                    <div class="col-md-6">Added: </div>
                    <div class="col-md-6" id="statsAdded"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-5">
                <div class="row hidden" id="statsUpdatedContainer">
                    <div class="col-md-6">Updated: </div>
                    <div class="col-md-6" id="statsUpdated"></div>
                </div>
            </div>
        </div>
        <div class="row" id="stopContainer">
            <div class="col-md-5">
                <button id="stop" type="button" class="btn hidden" action="new">Stop</button>
            </div>
        </div>
        
    </div>
</div>

