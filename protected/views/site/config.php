<?php
$db_cnf_path = Schema::getDbConfigPath();
$db_cnf = Schema::getDbConfig($db_cnf_path);
$tables_count = Schema::getTablesCount( Schema::getDbh($db_cnf), $db_cnf['db']); 
?>

<div id="config" class="container-fluid">
        <div class="row">
            <div class="col-12">
                
                <div class="row">
                    <div class="col-xs-12">
                        <h2>Connection Settings</h2>
                        <div id="confings-path"><?= $db_cnf_path ?></div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="row config-item">
                            <div class="col-md-6 text-left">Host: </div>
                            <div class="col-md-6"><?= (empty($db_cnf) ? '-' : '*****'. substr($db_cnf['host'], -2)) ?></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row config-item">
                            <div class="col-md-6 text-left">Database: </div>
                            <div class="col-md-6"><?= (empty($db_cnf) ? '-' : '*****'. substr($db_cnf['db'], -2)) ?></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row config-item">
                            <div class="col-md-6 text-left">User: </div>
                            <div class="col-md-6"><?= (empty($db_cnf) ? '-' : '*****'. substr($db_cnf['user'], -2)) ?></div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row config-item">
                            <div class="col-md-6 text-left">Password: </div>
                            <div class="col-md-6"><?= (empty($db_cnf) ? '-' : '*****'. substr($db_cnf['password'], -2)) ?></div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>

        <div class="row" style="margin: 25px 0;"></div>

        <div class="row">
            <div class="col-12">
                <h2>Status</h2>
            </div>
            <div class="col-12">
                <div style="min-height: 90px;">
                    <span id="statsConfig">Database is <?= ($tables_count == 0 ? 'not' : '') ?> installed</span>
                </div>
                <?php if ($tables_count == 0) : ?>
                <a id="btnInstallDB" href="#" class="btn btn-sm btn-primary">Install Database</a>
                <?php endif; ?>
            </div>
        </div>
</div>
