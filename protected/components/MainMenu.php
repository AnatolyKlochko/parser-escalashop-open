<?php
Yii::import('zii.widgets.CPortlet');
 
class MainMenu extends CPortlet
{
    public function init()
    {
        parent::init();
    }
 
    protected function renderContent()
    {
        $this->render('mainMenu');
    }
    
    /*
     * Verifies whether or not menu item is active.
     */    
    public function is_active(string $href)
    {
        return Yii::app()->request->requestUri === $href ? 'class="active"' : '';
    }
}
