<?php

$menu = [
    'Home' => '/',
    'Data' => '/data',
    'Config' => '/config'
];

?>
<ul class="nav navbar-nav"><?php
    foreach ($menu as $title => $href) { ?>
    <li <?= $this->is_active($href) ?>><a href="<?= $href ?>"><?= $title ?></a></li><?php
    } ?>
</ul>