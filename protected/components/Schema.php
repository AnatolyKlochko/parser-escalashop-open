<?php

/**
 * Provides simple interface to Database.
 */
class Schema
{
    public static $dbh;
    public static $db_cnf_path;
    public static $db_cnf;

    public static function getDbConfigPath()
    {
        global $config;
        if (empty(self::$db_cnf_path)) {
            self::$db_cnf_path = dirname($config) . '/schema-escala.php';
            return self::$db_cnf_path;
        }
        return self::$db_cnf_path;
    }
    
    public static function getDbConfig($path)
    {
        if (empty(self::$db_cnf)) {
            self::$db_cnf = require $path;
            return self::$db_cnf;
        }
        return self::$db_cnf;
    }
    
    public static function getDbh($db_cnf)
    {
        $mysqli = new mysqli($db_cnf['host'], $db_cnf['user'], $db_cnf['password'], $db_cnf['db']);
        
        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect to MySQL Server failed: %s\n", mysqli_connect_error());
            return null;
        }
        
        return $mysqli;
    }
    
    public static function getTablesCount($dbh, $db) : int
    {
        $sql = 'SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = \''.$db.'\';';
        $result = $dbh->query($sql)->fetch_array(MYSQLI_NUM);
        $tables_count = (int)$result[0];
        return $tables_count;
    }
}
