<?php
	
class Pattern {
    // Pattern types
    const TOP_CATEGORY_BLOCK = 1;   // extracts BLOCK with TOP Categories
    const TOP_CATEGORY = 2;         // one particular TOP Category (LINK)
    const CATEGORY_BLOCK = 3;       // 
    const CATEGORY = 4;             // 
    const WARES_LIST_LINK = 5;      // finds LINK to product list page | to next product list page (usually every product has image, title and price)
    const WARES_BLOCK = 6;          // extracts PRODUCTS BLOCK
    const WARE_LINK = 7;            // extracts LINK of one particular PRODUCT
    const WARE_BLOCK = 8;           // PRODUCT page. extracts BLOCK with PRODUCTS FEATURES
    const WARE_FEATURE = 9;         // it is IMAGE LINK, TITLE, PRICE, DESCRIPTION, etc
}
