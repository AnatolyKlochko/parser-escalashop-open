<?php

/**
 * Is able to export everything from DB to .xls file. Serious guy.
 */
class Exporter
{
    
    
    private $mysqli;
    private $supplier_id;

    
    
    public function __construct()
    {
        
    }

    public function ExportWaresAndCategoriesToXLS($supplier_id = null)
    {
        
        
        // Set SupplierID
        $this->supplier_id = $supplier_id;



        // MySQL connection
        $conn = $this->PrepareMySQLConnection();
        if(!$conn){
            echo 'Error: no MySQL connection. ExportToXLS() finished.';
            return false;
        }






        // Fill Excel book 


        // Fill 'Export Products Sheet' sheet


        // SQL string
        $sql = $this->GetSQLforWareTable();  


        // Get rows
        $ware_table = $this->mysqli->query($sql);
        if($ware_table === false){
            echo 'Error: until query perform by MySQL Server arise error: no result set. ExportToXLS() finished.<BR/>';
            printf("MySQL Server error description: %s\n", mysqli_error($this->mysqli));
            return false;
        }
        if($ware_table->num_rows == 0){
            echo 'Warning: Ware table has no records. ExportToXLS() finished.';
            return false;
        }




        // Excel. load template Excel Workbook
        $excel_book = PHPExcel_IOFactory::load(dirname(__FILE__) . '/export-ware-template.xls'); 








        // Excel. Get 'Export Products' sheet
        $export_prod_sheet = $excel_book->getSheetByName('Export Products Sheet');//  ->getSheet(0);


        // Fill cells

        // current row number (starts from 1)
        $row_num = 2;
        $supplier = new SupplierBase();
        //$feature_SIZE = array();
        //$feature = array();

        while ($ware_row = $ware_table->fetch_assoc()) {

            // CHECKING
            if(is_null($ware_row['Title'])){continue;}
            if(is_null($ware_row['KeyWords'])){continue;}
            if(is_null($ware_row['Description'])){continue;}
            if(is_null($ware_row['WareIdentifier'])){continue;}

            // CLEAR ware ID
            //$ware_id = 1;


            // Write Ware
            $this->WriteRowToExportProdSheetXLS($export_prod_sheet, $row_num, $ware_row, $supplier, $ware_id);

            // next row
            $row_num++;
            // next ware ID
            //$ware_id++;


            // Write Wares (РАЗНОВИДНОСТИ)
            /*$feature_SIZE =  $supplier->ProcessingFromDB_Size($ware_row['Size']);

            foreach($feature_SIZE as $size){

                $feature[0]['FEATURE_NAME'] = 'Размер';
                $feature[0]['FEATURE_MEASURE'] = '';
                $feature[0]['FEATURE_VALUE'] = $size;


                // Write line into XLS file
                $this->WriteRowToExportProdSheetXLS($export_prod_sheet, $row_num, $ware_row, $supplier, $ware_id, false, $feature);


                // next row
                $row_num++;

                // next Ware ID
                $ware_id++;

            }*/

        }

        // Free  ware_table  object
        $ware_table->close();






        // Fill 'Export Groups Sheet' sheet
        // Get 'Export Products Sheet' sheet
        $export_groups_sheet = $excel_book->getSheetByName('Export Groups Sheet'); 



        // table Category. SQL string
        $sql = $this->GetSQLforCategoryTable();  


        // Get rows
        $category_table = $this->mysqli->query($sql);
        if (! $category_table) {
            //echo 'Error: until query (to Category table) perform by MySQL Server arise error: no result set. ExportToXLS() finished.';
            return false;
        }
        if ($category_table->num_rows == 0) {
            //echo 'Warning: Category table has no records. ExportToXLS() finished.';
            return false;
        }

        $category_table_arr = $category_table->fetch_all(MYSQLI_ASSOC);
        $new_category_table_arr = $this->GetNewCategoryTableArray($category_table_arr);

        // current row number (starts from 1)
        $row_num = 2;

        // Fill 'Export Groups Sheet'
        foreach($new_category_table_arr as $key => $value){

            // Group Number (Номер_группы)
            $export_groups_sheet->setCellValue('A'.$row_num, $value['CategoryIDInPROMUA']);

            // Group Name (Название_группы)
            $export_groups_sheet->setCellValue('B'.$row_num, $value['Name']);

            // Group Identifier (Идентификатор_группы)
            $export_groups_sheet->setCellValue('C'.$row_num, $value['PROMUACategoryID']);


            // Parent (РОДИТЕЛЬ)
            $parent_id = $value['ParentCategoryID']; 

            // cheking: NO ELEMENT with index equal 0
            $exists = array_key_exists($parent_id, $new_category_table_arr); 

            if($exists){

                // Номер_родителя 
                $export_groups_sheet->setCellValue('D'.$row_num, $new_category_table_arr[$parent_id]['CategoryIDInPROMUA']);

                // Идентификатор_родителя
                $export_groups_sheet->setCellValue('E'.$row_num, $new_category_table_arr[$parent_id]['PROMUACategoryID']);

            }



            $row_num++;

        }



        // free Category table object
        $category_table->free();

        // Close MySQL connection
        $this->mysqli->close();
        


        // Save data to XLS file
        $writer = PHPExcel_IOFactory::createWriter($excel_book, 'Excel5');
        $file = dirname(__FILE__) . '/archive/' . date('Y-m-d') . '_' .date('H-i-s') . '-export-ware.xls';
        $writer->save($file);
        
        // Free objects
        $excel_book->disconnectWorksheets();
        unset($excel_book);
        
        
        return $file;
    }

    /**
     * Prepares & checks MySQL connection.
     */
    private function PrepareMySQLConnection()
    {
        // Get settings
        global $config;
        $db_cnf = require(dirname($config).'/schema-escala.php');
        
        // Create Connection
        $this->mysqli = new mysqli($db_cnf['host'], $db_cnf['user'], $db_cnf['password'], $db_cnf['db']);

        // Check connection
        if (mysqli_connect_errno()) {
            printf("Connect to MySQL Server failed: %s\n", mysqli_connect_error());
            return false;
        }


        return true;
    }

    /**
     * 
     */
    private function GetSQLforWareTable()
    {
        if (is_null($this->supplier_id)) {

             // SQL string for take all Wares ALL Suppliers
             $sql =  'SELECT WareID, Ware.SupplierID, Code, Title, Title2, KeyWords, Description, WareType, Price, Valuta, Discount, MeasureUnit, '.
                        'ImageFace, Image, Availability, Manufacturer, CountryManufacturer, '.
                        'MinQuantOrder, WholesalePrice, MinWholesaleOrder, DeliveryQuantity, DeliveryPeriod, PackagingWay, '.
                        'Category.CategoryIDInPROMUA, Category.PROMUACategoryLINK, WareIdentifier, UnicalIdentifier, '.
                        'Category.PROMUACategoryID, Category.CategoryID, Size, Color '. 
                    'FROM Ware LEFT JOIN (SupplierCategory, CategoryMap, Category) ON ('.
                        'Ware.SupplierCategoryID = SupplierCategory.SupplierCategoryID AND '.
                        'SupplierCategory.SupplierCategoryID = CategoryMap.SupplierCategoryID AND '.
                        'CategoryMap.CategoryID = Category.CategoryID) '.
                    'ORDER BY Ware.WareID '.
                    //'LIMIT 1 '.
                    '';
             $sql .= ';'; 

        } else {

            // DEBUG: DEBUG_Ware table. SQL string for take all Wares ONE Supplier
             /*$sql =  'SELECT WareID, DEBUG_Ware.SupplierID, Code, Title, Title2, KeyWords, Description, WareType, Price, Valuta, Discount, MeasureUnit, '.
                        'ImageFace, Image, Availability, Manufacturer, CountryManufacturer, '.
                        'MinQuantOrder, WholesalePrice, MinWholesaleOrder, DeliveryQuantity, DeliveryPeriod, PackagingWay, '.
                        'Category.CategoryIDInPROMUA, Category.PROMUACategoryLINK, WareIdentifier, UnicalIdentifier, '.
                        'Category.PROMUACategoryID, Category.CategoryID, Size, Color '. 
                    'FROM DEBUG_Ware LEFT JOIN (SupplierCategory, CategoryMap, Category) ON ('.
                        'DEBUG_Ware.SupplierCategoryID = SupplierCategory.SupplierCategoryID AND '.
                        'SupplierCategory.SupplierCategoryID = CategoryMap.SupplierCategoryID AND '.
                        'CategoryMap.CategoryID = Category.CategoryID) '.
                    'WHERE DEBUG_Ware.SupplierID = '. $this->supplier_id . ' ' . 
                    'ORDER BY DEBUG_Ware.WareID '.
                    'LIMIT 1 '.
                    '';
             $sql .= ';'; */


            // SQL string for take all Wares ONE Supplier
             $sql =  'SELECT WareID, Ware.SupplierID, Code, Title, Title2, KeyWords, Description, WareType, Price, Valuta, Discount, MeasureUnit, '.
                        'ImageFace, Image, Availability, Manufacturer, CountryManufacturer, '.
                        'MinQuantOrder, WholesalePrice, MinWholesaleOrder, DeliveryQuantity, DeliveryPeriod, PackagingWay, '.
                        'Category.CategoryIDInPROMUA, Category.PROMUACategoryLINK, WareIdentifier, UnicalIdentifier, '.
                        'Category.PROMUACategoryID, Category.CategoryID, Size, Color '. 
                    'FROM Ware LEFT JOIN (SupplierCategory, CategoryMap, Category) ON ('.
                        'Ware.SupplierCategoryID = SupplierCategory.SupplierCategoryID AND '.
                        'SupplierCategory.SupplierCategoryID = CategoryMap.SupplierCategoryID AND '.
                        'CategoryMap.CategoryID = Category.CategoryID) '.
                    'WHERE Ware.SupplierID = '. $this->supplier_id . ' ' . 
                    'ORDER BY Ware.WareID '.
                    //'LIMIT 1 '.
                    '';
             $sql .= ';'; 

        }

        return $sql;
    }

    /**
     * Returnt SQL expression to get all Categories.
     */
    private function GetSQLforCategoryTable()
    {
        // SQL string, table Category
        $sql =  'SELECT CategoryID, CategoryIDInPROMUA, Name, ParentCategoryID, PROMUACategoryID '. 
                'FROM Category '.
                'ORDER BY ParentCategoryID, Name '.
                //'LIMIT 10 '.
                '';
        $sql .= ';'; 


        return $sql;
    }

    /**
     * Writes a row to Excel Sheet.
     * 
     * $additional_feature array Is 2dim array. Every item is associative array which contain 3 elements: Название хар-ки, Измерение хар-ки, Значение хар-ки.
    */
    private function WriteRowToExportProdSheetXLS(&$export_prod_sheet, &$row_num, &$ware_row, &$supplier, &$ware_id, $ware_face = true, &$additional_feature = null)
    {
        // Код_товара
        $export_prod_sheet->setCellValue('A'.$row_num, $ware_row['Code']); 

        // Название_позиции
        $title = (is_null($ware_row['Title2']))? $ware_row['Title'] : $ware_row['Title2'];
        $export_prod_sheet->setCellValue('B'.$row_num, $title);

        // Ключевые_слова
        $export_prod_sheet->setCellValue('C'.$row_num, $ware_row['KeyWords']);

        // Описание
        $export_prod_sheet->setCellValue('D'.$row_num, $supplier->ProcessingFromDB_Description($ware_row['Description']));

        // Тип_товара	
        $export_prod_sheet->setCellValue('E'.$row_num, $ware_row['WareType']);

        // Цена	
        $export_prod_sheet->setCellValue('F'.$row_num, $ware_row['Price']);
        if(!is_null($ware_row['Price'])){
            // Валюта	
            $export_prod_sheet->setCellValue('G'.$row_num, $ware_row['Valuta']);
            // Скидка	
            $export_prod_sheet->setCellValue('H'.$row_num, $ware_row['Discount']);
            // Единица_измерения	
            $export_prod_sheet->setCellValue('I'.$row_num, $ware_row['MeasureUnit']);
        }

        // Ссылка_изображения	
        $export_prod_sheet->setCellValue('M'.$row_num, $ware_row['ImageFace'].', '.$ware_row['Image']);
        // Наличие	
        $export_prod_sheet->setCellValue('N'.$row_num, $ware_row['Availability']);
        // Производитель	
        $export_prod_sheet->setCellValue('O'.$row_num, $ware_row['Manufacturer']);
        // Страна_производитель	
        $export_prod_sheet->setCellValue('P'.$row_num, $ware_row['CountryManufacturer']);


        // Для ОПТОВИХ товаров
        // Минимальный_объем_заказа	
        $export_prod_sheet->setCellValue('J'.$row_num, $ware_row['MinQuantOrder']);
        // Оптовая_цена	
        $export_prod_sheet->setCellValue('K'.$row_num, $ware_row['WholesalePrice']);
        // Минимальный_заказ_опт	
        $export_prod_sheet->setCellValue('L'.$row_num, $ware_row['MinWholesaleOrder']);
        // Возможность_поставки	
        $export_prod_sheet->setCellValue('S'.$row_num, $ware_row['DeliveryQuantity']);
        // Срок_поставки	
        $export_prod_sheet->setCellValue('T'.$row_num, $ware_row['DeliveryPeriod']);
        // Способ_упаковки	
        $export_prod_sheet->setCellValue('U'.$row_num, $ware_row['PackagingWay']);


        // Идентификация товара
        // Номер_группы	
        $export_prod_sheet->setCellValue('Q'.$row_num, $ware_row['CategoryIDInPROMUA']);
        // Адрес_подраздела	
        $export_prod_sheet->setCellValue('R'.$row_num, $ware_row['PROMUACategoryLINK']);                
        // Идентификатор_товара	
        $export_prod_sheet->setCellValue('V'.$row_num, $ware_row['WareIdentifier']); //.'_'.$ware_id
        // Уникальный_идентификатор	
        $export_prod_sheet->setCellValue('W'.$row_num, $ware_row['UnicalIdentifier']);
        // Идентификатор_подраздела	
        $export_prod_sheet->setCellValue('X'.$row_num, $ware_row['PROMUACategoryID']); 
        // Идентификатор_группы	
        $export_prod_sheet->setCellValue('Y'.$row_num, $ware_row['CategoryID']);
        // ID_группы_разновидностей 
        //$export_prod_sheet->setCellValue('Z'.$row_num, $ware_row['WareGroupID']);    



        // ADDITIONAL FEATURES

        // Size
        if(!is_null($ware_row['Size'])){
            $export_prod_sheet->setCellValue('AA'.$row_num, 'Размер');
            $export_prod_sheet->setCellValue('AC'.$row_num, $supplier->ProcessingFromDB_Size($ware_row['Size']));
        }  

        // Color
        if(!is_null($ware_row['Color'])){  
            $export_prod_sheet->setCellValue('AD'.$row_num, 'Цвет');
            $export_prod_sheet->setCellValue('AF'.$row_num, $supplier->ProcessingFromDB_Color($ware_row['Color']));
        }  


        /*if(!$ware_face){

            // count of FEATURES
            $c = count($additional_feature); 

            // index of first FEATURE column - AA
            $col_num = 26; // number first feature column (by counting) is 27

            // cycle for FEATURES
            for($i=0;$i<$c;$i++){
                // cycle for FEATURE VALUES
                foreach($additional_feature[$i] as $key => $value){
                    $export_prod_sheet->setCellValueByColumnAndRow($col_num++, $row_num, $value);
                }

            }

        }*/



    }

    /**
     * 
     * 
    */
    private function GetNewCategoryTableArray(&$category_array)
    {
        $new_category_array = [];

        $c = count($category_array);

        for ($i=0; $i < $c; $i++) {

            $fields_arr = [];

            foreach ($category_array[$i] as $key => $value) {

                if($key == 'CategoryID'){ continue; }

                $fields_arr[$key] = $value;

            } 

            $new_category_array[$category_array[$i]['CategoryID']] = $fields_arr;//array($category_array[$i][1], $category_array[$i][2], $category_array[$i][3], $category_array[$i][4]);

        } 

        return $new_category_array;
    }


    
    private function DEBUG_testingCode()
    {
        // new Excel workbook
        $objPHPExcel = new PHPExcel();

        // where will be located objects of workbook
        $cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_in_memory;
        PHPExcel_Settings::setCacheStorageMethod($cacheMethod);

        // 4.6.5.	Locale Settings for Formulae
        $locale = 'ru';
        $validLocale = PHPExcel_Settings::setLocale($locale);
        if (! $validLocale) {
               echo 'Unable to set locale to '.$locale." - reverting to en_us<br />\n";
        }


        // When you instantiate a new workbook, PHPExcel will create it with a single worksheet called “WorkSheet”.
        // Note that sheets are indexed from 0
        $export_prod_sheet = $objPHPExcel->getSheet(0);
        // rename exist worksheet to "Export Products Sheet"
        $export_prod_sheet->setTitle('Export Products Sheet');

        // Create a new worksheet called “Export Groups Sheet”
        $export_groups_sheet = new PHPExcel_Worksheet($objPHPExcel, 'Export Groups Sheet');
        // Attach the “My Data” worksheet as the first worksheet in the PHPExcel object
        $objPHPExcel->addSheet($export_groups_sheet, 1);

        //$objPHPExcel->setActiveSheetIndex(0);
        $export_prod_sheet->setCellValue('A1', 'ware code 1');

        // 4.6.28.	Setting a column’s width
        //$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(12);
        //$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
        //default column width
        //$objPHPExcel->getActiveSheet()->getDefaultColumnDimension()->setWidth(12);
        // default row height
        //$objPHPExcel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(15);

        // 4.6.40.	Redirect output to a client’s web browser




        // 
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, date('YYYY-MM-DD').'-ware-export');
        $objWriter->save('archive.xls');


        // 4.3.	Clearing a Workbook from memory
        //$objPHPExcel->disconnectWorksheets();
        //unset($objPHPExcel);

    }
}
