<?php
	
class Supplier_4 extends SupplierBase
{
    const ID = 4;
    const MANUFACTURER = '';
    const MANUFACTURER_COUNTRY = '';

    
    
    
    
    public function __construct()
    {
        
    }





    /**
     * P R O C E S S    M E T H O D S
     */

    public function Processing_NextWaresListLINK($link)
    {
        static $current_category_link = '';
        static $current_page = 2;
        if ($current_category_link != $link) {
            $current_category_link = $link;
            $current_page = 2;  
        }

        return $current_category_link . '&page='.$current_page++;
    }


    /**
     * PROCESSING TO DB
     */

    /**
     * Note: Cyrilic symbols encode with 2 bytes...
     * 
    */
    public function ProcessingToDB_Title($title_raw)
    {  
        $title_raw = trim($title_raw);  


        // seek LAST ' ' in $title_raw
        $spacelast_pos = mb_strripos($title_raw, ' ', 0, 'UTF-8');

        // check result
        if($spacelast_pos === FALSE){ // not found
            return $title_raw;
        } else { // found
            $title = mb_substr($title_raw, 0, $spacelast_pos, 'UTF-8');  
        }

        return $title;
    }

    /**
      * Note: Cyrilic symbols encode with 2 bytes...
      * 
     */
    public function ProcessingToDB_Articul($articul_raw)
    {  
        $articul_raw = trim($articul_raw);  


        // seek LAST ' ' in $articul_raw
        $spacelast_pos = mb_strripos($articul_raw, ' ', 0, 'UTF-8');  

        // check result
        if ($spacelast_pos === FALSE) { // not found
            return NULL;
        } else { // found
            $articul_raw_length = mb_strlen($articul_raw, 'UTF-8');
            $articul = mb_substr($articul_raw, $spacelast_pos+1, $articul_raw_length-$spacelast_pos-1, 'UTF-8');  
        }

        return $articul;
    }
    public function ProcessingToDB_Price($price_raw)
    {
        $price_raw = trim($price_raw);  

        $price = '';
        if (is_string($price_raw)) {
            $c = mb_strlen($price_raw, 'UTF-8');
            for ($i = 0; $i < $c; $i++) {
                if (is_numeric($price_raw[$i]) OR $price_raw[$i]=='.') {
                    $price .= $price_raw[$i];
                }
            }
        } else {
            if (is_numeric($price_raw)) {
                return $price_raw;
            } else {
                return NULL;
            }
        }  

        if (is_numeric($price)) {
            return $price;
        } else {
            return NULL;
        }
    }
    public function ProcessingToDB_Availability($availability_raw)
    {  
        $availability_tmp = trim($availability_raw);

        // EXACTLY is
        $availis_pos = mb_stripos($availability_tmp, 'Есть в наличии', 0, 'UTF-8');            
        if ($availis_pos === FALSE) {
        } else {
            return '+';
        }

        // EXACTLY no
        $availnot_pos = mb_stripos($availability_tmp, 'Нет в наличии', 0, 'UTF-8');            
        if ($availnot_pos === FALSE) {
        } else {
            return '-';
        }

        // may be 
        // informer: 1.may error on site  2. may changed HTML structure -> change PATTERN
        return NULL;
    }
    public function ProcessingToDB_Image($image_raw)
    {
        // Availability & Sizes
        $image_result = '';
        $reg_exp = '/<a.*href="(?<Image>.+)".*>/isU';
        preg_match_all($reg_exp, $image_raw, $image_result, PREG_SET_ORDER);  
        // PREG_SET_ORDER - вхождення () в одной строке (в эл-тах одного массива)


        // check result
        if (count($image_result) > 0  AND  array_key_exists('Image', $image_result[0])) {
        } else {
            return NULL;
        }

        $images_pathes = '';

        $c = count($image_result);


        for ($i = 0; $i < $c; $i++) {
            // щоб не ставити ', ' перед першим ел-том
            if ($i == 0) { $images_pathes = $image_result[$i]['Image']; continue; }

            // за умовами Прому фото може бути мах 10 шт (1 FaceFoto + 9 Other).
            if ($i > 8) { continue; }

            $images_pathes .= ', ' . $image_result[$i]['Image'];
        }

        //echo var_dump($image_array);
        return $images_pathes;
    }
    public function ProcessingToDB_Size($size_raw_array)
    {
        if (is_null($size_raw_array)) {
            return NULL;
        }

        $c = count($size_raw_array);

        if ($c == 1) {
            $size = $size_raw_array[0];
        } else {
            $size = implode(';', $size_raw_array);
        }  

        return $size;
    }



    /**
     *  PROCESSING FROM DB
     */



}
  