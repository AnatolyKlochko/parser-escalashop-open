<?php
	
class Supplier_2 extends SupplierBase
{
    const ID = 2;
    const MANUFACTURER = '';
    const MANUFACTURER_COUNTRY = '';

    public function __construct()
    {
        
    }





    /**   P R O C E S S    M E T H O D S   */

   
    
    public function Processing_NextWaresListLINK($link)
    {
        static $current_category_link = '';
        static $current_page = 2;
        if ($current_category_link != $link) {
            $current_category_link = $link;
            $current_page = 2;  
        }

        return $current_category_link.'?page='.$current_page++;
    }



    
    /**
     * PROCESSING TO DB
     */
    
    
    public function ProcessingToDB_Title($title_raw)
    {
        $title = trim($title_raw);

        if (mb_strlen($title) > 0) {
            return $title;
        } else {
            return NULL;
        }
    }
    public function ProcessingToDB_Price($price_raw)
    {
        $price_raw = trim($price_raw);
        $pos_span = mb_stripos($price_raw, '<', 0, 'UTF-8');
        $price = mb_substr($price_raw, 0, $pos_span, 'UTF-8');  

        if (mb_strlen($price) > 0) {
            return $price;
        } else {
            return NULL;
        }
    }
    public function ProcessingToDB_Valuta($valuta_raw)
    {  
        $valuta_raw = trim($valuta_raw);
        if ($valuta_raw == 'грн') {
            return 'UAH';
        } else {
            return NULL;
        }
    }
    public function ProcessingToDB_Description($description_raw)
    {
       $description_raw = '<ul>'.$description_raw.'</ul>';
       $description = htmlentities($description_raw); 
       if (mb_strlen($description) > 0) {
            return $description;
        } else {
            return NULL;
        }
    }
    public function ProcessingToDB_Availability($availability_raw)
    {  
        // seek 
        $result = mb_stripos($availability_raw, 'productPresent', 0, 'UTF-8');  

        // check result
        if ($result === FALSE) {
            return '-';
        } else {
            return '+';
        }
    }
    public function ProcessingToDB_Size($size_raw)
    {
        // Availability & Sizes
        $amount_size_result = '';
        $reg_exp = '/<span.*data-ms-amount.*"(?<amount>.+)">(?<size>.+)<\/span>/isU';
        preg_match_all($reg_exp, $size_raw, $amount_size_result, PREG_SET_ORDER);  
        // PREG_SET_ORDER - вхождения () в одной строке (в эл-тах одного массива)


        // check result
        if(count($amount_size_result)>0 AND array_key_exists('amount', $amount_size_result[0]) AND array_key_exists('size', $amount_size_result[0])) {
        } else {
            return NULL;
        }


        // get size
        $available_size = array();
        $c = count($amount_size_result);
        for ($i=0; $i < $c; $i++) {
            if ($amount_size_result[$i]['amount'] > 0) {
                if (!in_array($amount_size_result[$i]['size'], $available_size)) {
                   $available_size[] = $amount_size_result[$i]['size'];
                }
            }
        }
        $c = count($available_size);
        for ($i=0; $i < $c; $i++) {
            $available_size[$i] = $available_size[$i] . ' (' . SupplierBase::GetRusSIZE($available_size[$i]) . ')'; 
        }

        if ($c == 1) {
            $size = $available_size[0];
        } else {
            $size = implode(';', $available_size);
        }              

        return $size;
    }
    public function ProcessingToDB_Color($color_raw)
    {  
        $color_string = '';

        foreach ($color_raw as $key => &$value) {
            $value = trim($value);
            if (mb_stripos($color_string, $value, 0, 'UTF-8') === FALSE) {
                $color_string .= $value.';';
            }
        }              

        $len = mb_strlen($color_string, 'UTF-8');  
        if ($len > 0) {
            $color_string = mb_substr($color_string, 0, $len-1, 'UTF-8');  
            return $color_string;
        } else {
            return NULL;
        }
    }



    /**
     * PROCESSING FROM DB 
     */




}