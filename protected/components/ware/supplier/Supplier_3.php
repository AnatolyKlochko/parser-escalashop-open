<?php
	
class Supplier_3 extends SupplierBase
{
    const ID = 3;
    const MANUFACTURER = '';
    const MANUFACTURER_COUNTRY = '';

    public function __construct()
    {
        
    }





    /**
     *  P R O C E S S    M E T H O D S
     */

   
    
    public function Processing_NextWaresListLINK($link)
    {
        static $current_category_link = '';
        static $current_page = 2;
        if($current_category_link != $link){
            $current_category_link = $link;
            $current_page = 2;  
        }

        return $current_category_link . 'page/'.$current_page++.'/?sort=new_first';
    }


    /**
     *  PROCESSING TO DB
     */
    
    
    
    public function ProcessingToDB_Availability($availability_raw)
    {  
        $availability_tmp = trim($availability_raw);

        // EXACTLY is
        $availis_pos = mb_stripos($availability_tmp, 'В наличии', 0, 'UTF-8');            
        if ($availis_pos === FALSE) {
            //return '-';
        } else {
            return '+';
        }

        // EXACTLY no
        $availnot_pos = mb_stripos($availability_tmp, 'Нет в наличии', 0, 'UTF-8');            
        if ($availnot_pos === FALSE) {
        } else {
            return '-';
        }

        // may be 
        // informer: 1.may error on site  2. may changed HTML structure -> change PATTERN
        return NULL;
    }
    public function ProcessingToDB_ImageFace($imageface_raw)
    {  
        $imageface_raw = trim($imageface_raw);

        if ($imageface_raw[0] == '/' && $imageface_raw[1] == '/') {
            return 'http:'.$imageface_raw;
            //return mb_substr($imageface_raw, 2, mb_strlen($imageface_raw, 'UTF-8')-2, 'UTF-8');
        } else {
            return $imageface_raw;
        }
    }
    public function ProcessingToDB_Image($image_array)
    {
       $images_pathes = '';

       $c = count($image_array);

       if ($c > 0) {
            for ($i = 0; $i < $c; $i++) {

                $image_array[$i] = self::ProcessingToDB_ImageFace($image_array[$i]);  

                if ($i == 0) { $images_pathes = $image_array[0]; continue; }

                // за умовами Прому фото може бути мах 10 шт (1 FaceFoto + 9 Other).
                if ($i > 8) { continue; }

                $images_pathes .= ', ' . $image_array[$i];
            }
       }
       //echo var_dump($image_array);
       return $images_pathes;
    }




    /**
     *  PROCESSING FROM DB
     */




}
