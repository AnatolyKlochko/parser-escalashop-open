<?php

/**
 * A shop supplier.
 */
class Supplier_1 extends SupplierBase
{
    const ID = 1;
    const MANUFACTURER = '5.3 Mission';
    const MANUFACTURER_COUNTRY = 'Украина';



    public function __construct()
    {
        
    }




    /**
     * P R O C E S S    M E T H O D S
     */
   

   /**
    * Returns LINK for next PRODUCT LIST page
    */
    public function Processing_NextWaresListLINK($link)
    {
        static $current_category_link = '';
        static $current_page = 2;
        if($current_category_link != $link) {
            $current_category_link = $link;
            $current_page = 2;  
        }

        return $current_category_link . '?p='.$current_page++;
    }




    /**
     * PROCESSING TO DB  (before value will be saved in Database)
     */
   
   /**
    * Extracts SIZE value from raw size expression.
    */
    public function ProcessingToDB_Size($size)
    {
        $reg_exp = '/<option.*>(?<Size>.+)<\/option>/isU';
        $preg_result = '';
        preg_match_all($reg_exp, $size, $preg_result, PREG_SET_ORDER);
        // PREG_SET_ORDER - вхождения () в одной строке (в эл-тах одного массива)

        // check result
        if (count($preg_result) > 0 AND array_key_exists('Size', $preg_result[0])) {
        } else {
            return null;
        }


        $values = array();        
        $c = count($preg_result);
        for ($i=0; $i < $c; $i++) {
            // if string contains spaces on start or end, cut it
            $values[] = trim($preg_result[$i]['Size']);
        }



        $sizes = array();
        $dash_pos = 0;
        $c = count($values); 

        for ($i=0; $i < $c; $i++) {
            // empty element
            if ($values[$i] == '---') { 
                continue;
            }


            // elements separated with dash:  42-44-46-48
            $dash_pos = stripos($values[$i], '-'); 
            if ($dash_pos === false) { 

                // if no dash at row: then it is just number - SIZE
                if (is_numeric($values[$i])) {
                    $sizes[] = $values[$i];
                }


            } else { 

                // dash is present at row: 
                $tmp_size = explode('-', $values[$i]); 
                foreach ($tmp_size as &$value) {
                    $value = trim($value); 
                    if (is_numeric($value)) { 
                        $sizes[] = $value;
                    }
                }
                
            }
            
        }


        $c = count($sizes);
        if ($c == 1) {
            $size = $sizes[0];
        } else {
            $size = implode(';', $sizes);
        }  

        return $size;
    }

    /**
     * Extracts Title2 (extended title) value from raw title expression.
     */
    public function ProcessingToDB_Title2($title)
    {
        //$title_tmp = htmlentities($title); // replaces: ', ", <, >
        $false_count = 0;
        if (stripos($title, "&") === false) { $false_count++; }
        if (stripos($title, "<") === false) { $false_count++; }
        if (stripos($title, ">") === false) { $false_count++; }

        if ($false_count == 3) {
            return $title;
        } else {
            return null;
        }
   }
   
    /**
     * Extracts Availability value from raw availability expression.
     */
    public function ProcessingToDB_Availability($availability_raw)
    {
        $availability_tmp = trim($availability_raw);
        if ($availability_tmp == 'Нет в наличии') {
            return '-';
        } elseif ($availability_tmp == 'Есть в наличии' OR $availability_tmp == 'Новый' OR $availability_tmp == 'Хит продаж' OR $availability_tmp == 'Акционный') {
            return '+';
        } else {
            return null;
        }
    }

    /**
     * Extracts Articul value from raw articul expression.
     */
    public function ProcessingToDB_Articul($articul_raw)
    {
        $articul_raw = trim($articul_raw);
        $articul_raw_length = mb_strlen($articul_raw, 'UTF-8');
        $pos = mb_strripos($articul_raw, ' ', 0, 'UTF-8');
        $articul_raw = mb_substr($articul_raw, $pos + 1, $articul_raw_length - $pos - 1, 'UTF-8');


        if ($articul_raw) {
            return $articul_raw;
        } else {
            return 'null';
        }

    }

    
    
    
    
    /**
     * PROCESSING FROM DB
     */
    
    
    
}
