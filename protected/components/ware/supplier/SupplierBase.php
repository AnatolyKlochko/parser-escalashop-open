<?php
	    
class SupplierBase
{
    /**
     * Scans supplier site for new products.
     */
    public function AddNewSupplierWares()
    {
        $parser = new Parser($this);
        $parser->AddNewSupplierWares();
        unset($parser);
    }

    /**
     * Downloads, parses and writes a product features to Database. A PRODUCT LINKs 
     * takes from `SupplierWare` table.
     */
    public function AddNewWares()
    {
         $parser = new Parser($this);
         $parser->AddNewWares();
         unset($parser);
    }

    /**
     * Updates all exist products and all their features (rechecked all Database).
     */
    public function UpdateWares()
    {
                
    }





    /**
     *  P R O C E S S    M E T H O D S
     */
    
    
    public function Processing_NextWaresListLINK($current_link)
    {
        
    }

    /**
     * A cleanLINK is a LINK without ID, params, etc
     * 
     * Examples: 
     *   http://www.karree.com.ua/products/957#C24  ->  http://www.karree.com.ua/products/957
     *   http://www.karree.com.ua/products/957#C24?m=1  ->  http://www.karree.com.ua/products/957
     * 
    */ 
    public function Processing_cleanLINK($link_raw)
    {
        $pos_sharp = mb_stripos($link_raw, '#', 0, 'UTF-8');  

        if ($pos_sharp === FALSE) {

            return $link_raw;

        } else {

            $link = mb_substr($link_raw, 0, $pos_sharp, 'UTF-8');  

        }

        if (mb_strlen($link) > 0) {
            return $link;
        } else {
            return NULL;
        }
    }


    

    
    /**
     *  PROCESSING TO DB
     */
    
    
    public function ProcessingToDB_Image($image_array)
    {
        $images_pathes = '';

        $c = count($image_array);

        if ($c > 0) {
             for ($i = 0; $i < $c; $i++) {
                 // щоб не ставити ', ' перед першим ел-том
                 if ($i == 0) { $images_pathes = $image_array[0]; continue; }

                 // за умовами Прому фото може бути мах 10 шт (1 FaceFoto + 9 Other).
                 if ($i > 8) { continue; }

                 $images_pathes .= ', ' . $image_array[$i];
             }
        }
        //echo var_dump($image_array);
        return $images_pathes;
    }
    public function ProcessingToDB_Description($description_string)
    {
        return htmlentities($description_string);
    }



    
    
    /**
     *  PROCESSING FROM DB
     */
       
    
    public function ProcessingFromDB_Description($description)
    {
        return html_entity_decode($description);
    }
    public function ProcessingFromDB_Size($size)
    {  
        $size = explode(';', $size);

        $size_string = '';
        foreach ($size as $key => $value) {
            $size_string .= $value.', ';
        }

        $len = mb_strlen($size_string, 'UTF-8');
        $size_string = mb_substr($size_string, 0, $len-2, 'UTF-8');  

        return $size_string;
    }
    public function ProcessingFromDB_Color($color)
    {  
        $color = explode(';', $color);


        $color_string = '';
        foreach ($color as $key => $value) {
            $color_string .= $value.', ';
        }  


        $len = mb_strlen($color_string, 'UTF-8');
        $color_string = mb_substr($color_string, 0, $len-2, 'UTF-8');  


        return $color_string;
    }
    public function ProcessingFromDB_Link($link)
    {
        return html_entity_decode($link);
    }


    

    
    /** 
     * Helper Methods
     */
    
    
    public static function GetRusSIZE($euro_size)
    {
        switch ($euro_size) {
            case 'XS': return '40-42';
            case 'S': return '42-44';
            case 'M': return '44-46';
            case 'L': return '46-48';
            case 'XL': return '48-50';
            case 'XXL': return '50-52';
            default: return NULL;
        }
    }
    public static function GetEuroSIZE($rus_size)
    {
        
    }

    /**
     * Switch
     * 
     * @param type $supplier_id
     * @param type $feature_name
     * @param type $feature_value
     * @return type 
     */
    public static function ProcessFeature(&$supplier_id, &$feature_name, &$feature_value)
    {
        switch ($supplier_id) {

            // 53mission.com
            case 1:
                switch ($feature_name) {
                    case 'Image':
                        $s = new Supplier_1();
                        return $s->Processing_Image($feature_value);
                }
                break;

            // Any another
            default:
                return NULL;
        }
    }
       
}
