<?php

function ware_autoloader($class_name)
{
    // WARNING: add here first letter of the class name. Overwise this autoloader can't load class from  /components/ware  catalog
    switch ($class_name[0]) {
        case 'P':
            break;
        
        case 'S':
            break;
        
        case 'E':
            break;

        default:
            return false;
    }
    
    if ($class_name == 'Parser') {
        include_once(dirname(__FILE__).'/Parser.php');
        return class_exists($class_name);
    }


    if ($class_name == 'Pattern') {
        include_once(dirname(__FILE__).'/Pattern.php');
        return class_exists($class_name);
    }


    if ($class_name == 'SupplierBase') {
        include_once(dirname(__FILE__).'/supplier/SupplierBase.php');
        return class_exists($class_name);
    }
    
    
    if (strlen($class_name) > 9) {
        if (substr($class_name,0,9) == 'Supplier_') {
            include_once(dirname(__FILE__).'/supplier/' . $class_name . '.php');
            return class_exists($class_name);
        }
    }

    
    if ($class_name == 'Exporter') {
        include_once(dirname(__FILE__).'/exporter/Exporter.php');
        return class_exists($class_name);
    }
    

    return false;
}

spl_autoload_register('ware_autoloader');
