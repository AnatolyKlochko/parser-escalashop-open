<?php

/**
 * Requirements:
 * max_time_script_execution (PHP)
 * max_query_connections (MySQL)
 * max_memory_limit (PHP)
 * 
 * 
 * Security:
 * - table 'SupplierCategory' contains link of first category page with products list. It is added by hand (no SQL-injections).
 *      - extract supplier product links (in 'GetSupplierWare' method)
 *          - add new supplier products to DB (in 'AddSupplierWareToDB')
 *            here product links are handle to remove SQL-injections
 *      - extract next products page link (in 'GetSupplierWare'>>'GetNextSupplierWaresListLINK' method)
 * - select all rows of particular supplier from 'SupplierWare' table, use LINK for download product page
 * - extract all product feature and add they to DB. There in EVERY feature value can be SQL-injection, and if
 *   feature contains HTML tags, can be JS-injections.
 *   here EVERY feautre value is cleaned from HTML tags (with strip_tags func) :  and : lines, and product is added
 *   to DB with prepared query (to avoid SQL-injection) : line.
 * - future: if values from some fields will use in SQL expressions, then to avoid secondary SQL-injection need:
 *      1) or every time use prepared queries
 *      2) or to care that every value has no SQL-injection
 * 
*/

class Parser
{
    private $mysqli;            // MySQL connection object
    private $db;                // used by AddWareToDB method
    private $supplier_link;     // without slash on end
    private $supplier;          // object of Supplier_* class
    
    // Statistics
    private $product_lookup = 0;
    private $product_new = 0;
    private $product_added = 0;
    private $statistics;
    




    public function __construct($supplier = null)
    {
        if (! empty($supplier))
            $this->supplier = $supplier;
        
        // Statistics
        $this->statistics = [
            'result' => true,
            'message' => 'Initialization',
            'start' => microtime(true),
            'data' => [
                'lookups' => 0,
                'new' => 0
            ]
        ];
                
        $this->GetMySQLConnection();
        
    }
    
    /**
     * Updates products one particular Supplier or all Suppliers (if $SupplierID equal 0).
     */
    public function Start(int $SupplierID = 0)
    {
        
        // Set max_execution_time
        //ini_set('max_execution_time', 9000);
        
        
        // prepare DB connection
        $sql = '';
        $this->SQLString_Supplier($sql, $SupplierID); // returns one particular or all suppliers
        
        
    
        
        // get Suppliers (from Supplier table)
        $result = array();
        if ($result = $this->mysqli->query($sql)) {
            
            while ($row = $result->fetch_assoc()) {
                $this->ScanSupplier($row);
            }
            
            // free result set
            $result->close();
        }
        
        // free mysql connection
        $this->mysqli->close();
        
    }
    
    /**
     * Prepare SQL query string
     * 
     * @return string Returns only one supplier or all supplier (if $SupplierID < 1).
    */
    private function SQLString_Supplier(&$sql, &$SupplierID)
    {
        $sql = 'SELECT SupplierID, SiteLINK, TopCategoryListLINK FROM Supplier ';
        if ($SupplierID > 0) {
            $sql .= 'WHERE SupplierID='.$SupplierID.';';
        } else {
            $sql .= 'ORDER BY SupplierID ASC;';
        }
        //return $sql;
    }
    
    /**
     * Prepare & check MySQL connection
    */
    private function GetMySQLConnection()
    {
        global $config;
        $db_cnf = require(dirname($config).'/schema-escala.php');
        
        $this->mysqli = new mysqli($db_cnf['host'], $db_cnf['user'], $db_cnf['password'], $db_cnf['db']);
        
        /* check connection */
        if (mysqli_connect_errno()) {
            printf("Connect to MySQL Server failed: %s\n", mysqli_connect_error());
            exit();
        }
        
        $this->db = $db_cnf['db'];
    }
    
    private function writeStatistics(array $statsArr, $action)
    {
        $resultStats = array_merge($this->statistics, $statsArr);
        Statistics::setData($action, $resultStats);
    }
    private function statsStopArray()
    {
        return [
            'result'=>true, 
            'message'=>'Stopped', 
            'data' => [
                'lookups' => $this->product_lookup,
                'new' => $this->product_new,
                'added' => $this->product_added
            ],
            'finish'=> microtime(true),
            'time' => microtime(true) - $this->statistics['start']
        ];
    }
	
	
	
    
    
    /**
     *  S U P P L I E R
     */

    
    /**
     * Scanning supplier site
     * $row is associative array that represent one single row from Supplier table (is a one supplier)
    */
    private function ScanSupplier(&$supplier_row)
    {
        // create Supplier object
        $supplier = 'Supplier_' . $SupplierID;
        $this->supplier = new $supplier();
        
        
        
        // write Supplier LINK to local property
        $this->PrepareSupplierLINK($supplier_row['SiteLINK']);
        		
        
        /* check on exist LINK to TOP Category */
//        if(is_null($supplier_row['TopCategoryListLINK'])){ 
//            // note ...
//            return; 
//        }
        
        // Categories
        //$this->ScanSupplierCategory($supplier_row);
        
        
        
        // scan wares lists: add new Supplier wares to SupplierWare table
        $this->AddNewSupplierWares($supplier_row);
        
        
        
        // add new Supplier wares to OUR Ware table
        $this->AddNewWares($supplier_row);
        
        
        
        // unset Supplier object
        unset($this->supplier);
        
    }
    
    /**
    * Removes '/' if are last, and assign  $supplier_link  property
    */
    private function PrepareSupplierLINK(&$link)
    {
        if (isset($link) && mb_strlen($link, 'UTF-8') > 0) { //may be check encoding
        } else {
             //echo 'FATAL ERROR: no SiteLINK.';
             exit();
        }

        $schema_pos = mb_stripos($link, '://', 0, 'UTF-8');
        if ($schema_pos === FALSE) {
             $last_slash = mb_stripos($link, '/', 0, 'UTF-8');
        } else {
             $last_slash = mb_stripos($link, '/', $schema_pos+3, 'UTF-8');
        }
        if ($last_slash === FALSE) {
             $this->supplier_link = $link;
        } else {
             $this->supplier_link = mb_substr($link, 0, $last_slash, 'UTF-8');
        }
    }
    
    /**
     * Supplier data.
     * 
     * @return array
     */
    private function GetSupplierRow()
    {
        $id = $this->supplier::ID;
        $supplier_data = $this->mysqli->query('SELECT * FROM supplier WHERE SupplierID = ' . $id)->fetch_assoc();
                
        return $supplier_data;
    }





    /**
     * S U P P L I E R   C A T E G O R Y
     */

    
    /**
     * Is not implemented.
     */
    private function ScanSupplierCategory(&$row)
    {
        // get html, apply patterns, get TOP Categories, write it to DB
        $this->GetTopSupplierCategory($row['SupplierID'], $row['TopCategoryListLINK']);
        //return;
        // get TOP Categories (from DB), get html, apply patterns, get Categories, write it to DB
        $this->GetSupplierCategory($row['SupplierID']);    
    }
    
    /**
     * Finds (parses) the Top Category, and add it to DB
    */
    private function GetTopSupplierCategory(&$supplier_id, &$link)
    {
        /*?can change $pattern to array()*/
        $pattern_block_id = 0;
        $pattern_block = '';
        $pattern = '';
        $preg_result = array();
        $category_block_html = '';
        //$category = array();
      
        /* take pattern  for TOP Category block (from Pattern table) */
        /* здесь можна одним запросом обойтись where id= and a = (select ...) */
        /*на PatternType сделать подзапрос with ~like'top_cat*' ?если не изменится структура */
        $row = $this->GetPattern($supplier_id, null, null, 1);
        $pattern_block_id = $row['PatternID'];
        $pattern_block = $row['Text'];
                           
        /* take pattern  for TOP Category */
        $row = $this->GetPattern($supplier_id, null, $pattern_block_id, 2);
        $pattern = $row['Text'];
                
        
        /* get html of page */
        $html = '';
        $html = @file_get_contents($link);
        if(strlen($html) < 10){
            //echo 'Error: No HTML (empty page for '.$link.').<br/>';
            return false;
        }
        
        
        /* TOP CATEGORY BLOCK */
        $category_block_html = $this->ApplyPatternByPREG_MATCH($pattern_block, $html, $preg_result);    
        
        
        /* TOP CATEGORIES */
	$this->ApplyPatternByPREG_MATCH_ALL($pattern, $category_block_html, $preg_result);
        
        
        /* add found Categories to DB (SupplierCategory table) */
        $this->AddSupplierCategoryToDB($preg_result, $supplier_id);
                
    }
    
    /**
     * This function work ONLY WITH sites which page structure IDENTICAL like 53mission.com.
     * For work with ANY other sites need  add field LEVEL in SupplierCategory tables: for every level of category will be own particular pattern.
     * And need count current level and transmission his to next func calling.
    */
    private function GetSupplierCategory(&$supplier_id, $parent_category_id = null, $recursive = false)
    {
        // prepare SQL string to get parent Categories
        $sql = 'SELECT SupplierCategoryID, LINK FROM SupplierCategory WHERE SupplierID='.$supplier_id.' AND NotCategory=0';
        if (!is_null($parent_category_id)) { 
            $sql .= ' AND ParentCategoryID='.$parent_category_id; 
        } else { 
            $sql .= ' AND ParentCategoryID=0';
        }
        $sql .= ';';
        
        $result = array();
        if ($result = $this->mysqli->query($sql)) {
            // find sub Categories
            while ($parent_category_row = $result->fetch_assoc()) {
                
                /* take pattern  for Categories block (from Pattern table) */
                $row = $this->GetPattern($supplier_id, null, null, 3);
                $pattern_block_id = $row['PatternID'];
                $pattern_block = $row['Text'];
                
                /* take pattern  for one Category */
                $row = $this->GetPattern($supplier_id, null, $pattern_block_id, 4);
                $pattern = $row['Text'];
                
        
                /* get html of page */
                $html = '';
                $html = @file_get_contents($parent_category_row['LINK']);
                if(strlen($html) < 10){
                    //echo 'Error: No HTML (empty page for '.$parent_category_row['LINK'].').<br/>';
                    return false;
                }
                
        
                /* TOP CATEGORY BLOCK */
                $category_block_html = $this->ApplyPatternByPREG_MATCH($pattern_block, $html, $preg_result);    
                
        
                /* TOP CATEGORIES */
                $this->ApplyPatternByPREG_MATCH_ALL($pattern, $category_block_html, $preg_result);
                
        
                /* add found Categories to DB (SupplierCategory table) */
                $this->AddSupplierCategoryToDB($preg_result, $supplier_id, $parent_category_row['SupplierCategoryID']);
        
                
                
                // recursive search. Uncomment below to on.
                /*if($recursive){
                    $this->GetCategory($supplier_id, $parent_category_row['SupplierCategoryID'], $parent_category_row['LINK'], true);
                }*/
            }
            
            /* free result set */
            $result->close();
        }
        
    }
	
    /**
     * 
    */
    private function PrepareSupplierCategory_AfterPREG_MACTH_ALL(&$preg_result)
    {
        // check $preg_result: Categories exist?
        if (! (count($preg_result) > 0)) {
            //echo 'no Categories found. -> change PATTERN for ...';
            // : no Categories. May be HTML-struct was changed -> check Pattern
            return false;
        }
        if (! (isset($preg_result[0]['link']) AND isset($preg_result[0]['name']))) {
            //echo 'no features LINK or NAME (of Category)';
            // write note to table SupplierCategory
            return false;
        }
        
         if (! (strlen($preg_result[0]['link'])>1 AND strlen($preg_result[0]['name'])>1)) {
            //echo 'features LINK or NAME of Category are EMPTY';
            // write note to table SupplierCategory
            return false;    
        }
        
        
            
        $c = count($preg_result);
        
        // check links: if uses relative links change it to full links
        for ($i = 0; $i < $c; $i++) {
            // if string contains spaces on start or end, we cut it
            $preg_result[$i]['name'] = trim($preg_result[$i]['name']);
            $preg_result[$i]['link'] = trim($preg_result[$i]['link']);
            
            // prepare link: if uses relative links, add to them site link
            if ($preg_result[$i]['link'][0] == '/') {
                $preg_result[$i]['link'] = $this->supplier_link . $preg_result[$i]['link'];
            }
        }
        
        return true;
    }
    
    /**
     * 
    */
    private function AddSupplierCategoryToDB(&$preg_result, $supplier_id, $parent_category_id=0)
    {
        $checking = $this->PrepareSupplierCategory_AfterPREG_MACTH_ALL($preg_result);
        if (! $checking) { return; }
              
        
        
        /* add ONLY new categories */
        
        
              
        // prepare SQL string    
        $sql_values = '';
        $c = count($preg_result);    
        for ($i = 0; $i < $c; $i++) {
            /* check on exist in DB one current Category */
            $sql = "SELECT LINK FROM SupplierCategory WHERE LINK='" . $preg_result[$i]['link'] . "';";
            $one_category_result = '';
            if ($one_category_result = $this->mysqli->query($sql)) {
                if ($one_category_result->num_rows > 0) {
                    continue;
                } else {
                    (strlen($sql_values) == 0) ? '' : $sql_values.=',';
                }
            } else { 
                return; 
            }
            
            /* prepare SQL string */
            $sql_values .= "(null," . $supplier_id . ",". $parent_category_id . ",'" . $preg_result[$i]['name'] . "','" . $preg_result[$i]['link'] . "')";
            
        }
        
        /* add new Category to DB */
        $sql = 'INSERT INTO SupplierCategory(SupplierCategoryID, SupplierID, ParentCategoryID, Name, LINK) ' .
                'VALUES '. $sql_values . ';';
        if (! ($this->mysqli->query($sql))) {
            /* if occurs error */
            // write in Supplier table message about...
            return false;
        }
		
        return true;
    }
	
    
    
    
    
    /**
     *   S U P P L I E R   W A R E
     */
    
    
    /**
     * Заполняет таблицу SupplierWare (получает линки "последних" Категорий из 
     * таблицы SupplierCategory и сканирует странички виделяя линки товаров)
     * Add only new supplier products (always 'update mode').
    */
    public function AddNewSupplierWares(&$supplier_row = null)
    {
        if (empty($supplier_row))
            $supplier_row = $this->GetSupplierRow();
        
        
        // ID
        $sID = &$supplier_row['SupplierID'];
        
        
        // Statistics
        $this->statistics['message'] = 'Parsing (supplier id - '.$sID.')...';
        $this->writeStatistics([
            'data' => [
                'lookups' => $this->product_lookup,
                'new' => $this->product_new
            ]
        ], 'new');
        
        
        
        
        // get "last" Categories
        // SELECT * FROM Category WHERE CategoryID NOT IN (SELECT ParentCategoryID FROM Category GROUP BY ParentCategoryID);
        
        
        
        $sql =  'SELECT SupplierCategoryID, LINK, ParentCategoryID '.
                'FROM SupplierCategory '.
		'WHERE SupplierID = '.$sID." AND MultiCategory <> b'1' " .  //  AND SupplierCategoryID NOT IN (SELECT ParentCategoryID FROM SupplierCategory GROUP BY ParentCategoryID)
		'ORDER BY ParentCategoryID DESC ' .
		//'LIMIT 100 '.
		'';
        //if(!is_null($parent_category_id)){ $sql .= ' AND ParentCategoryID='.$parent_category_id; } else { $sql .= ' AND ParentCategoryID=0'; }
        $sql .= ';';  
        
        
        $result = $this->mysqli->query($sql);
        if ($result === FALSE) {
            //note no 'last' Categories
            //echo 'ERROR: on MySQL Server side. Reasons: wrong SQL string, no connection, MySQL Server not run, ...';
            
            return false;
        } 
        if ($result->num_rows == 0) {
            //note no 'last' Categories
            //echo "ERROR: no 'last' Categories for Supplier ID ".$sID;
            
            return false;
        }
        
        
        
        // TODO here:  get PATTERNS for wares_block and ware_link; methods for get patterns move to Pattern class
        
                
        
        
        
        // cycle for all "last" Categories ONE particular Supplier
        // call GetWare() which scan ALL pages ONE particular Category
        while ($category_row = $result->fetch_assoc()) {
            // Stop
            if (Statistics::stopStatus('new')) {
                $this->writeStatistics($this->statsStopArray(), 'new');
                return;
            }
            
            
            // Parses supplier
            $this->GetSupplierWare($supplier_row, $category_row, $category_row['LINK']);
            
        }
        
        
        
        
        
        // Scan MultiCategory like Новинки, Хит продаж и т.п.
        
        // SQL query string
        $sql =  'SELECT SupplierCategoryID, LINK, ParentCategoryID '.
                'FROM SupplierCategory '.
		'WHERE SupplierID='.$sID." AND MultiCategory=b'1' " .  //  AND SupplierCategoryID NOT IN (SELECT ParentCategoryID FROM SupplierCategory GROUP BY ParentCategoryID)
		'ORDER BY ParentCategoryID DESC ' .
		//'LIMIT 100 '.
		'';
        
        // performing query
        $result = $this->mysqli->query($sql);
        
        // check result
	if ($result === FALSE) {
            //note no 'last' Categories
            //echo 'ERROR: on MySQL Server side. Reasons: wrong SQL string, no connection, MySQL Server not run, ...';
            
            return false;
        } 
        if ($result->num_rows == 0) {
            //note no 'last' Categories
            //echo "ERROR: no 'MultiCategories' for Supplier: ID ".$sID;
            
            return false;
        }
        
        // cycle
        while ($category_row = $result->fetch_assoc()) {
            // Stop
            if (Statistics::stopStatus('new')){
                $this->writeStatistics($this->statsStopArray(), 'new');
                return;
            }
            
            
            // Parses supplier
            $this->GetSupplierWare($supplier_row, $category_row, $category_row['LINK']);
        }
        
        
        
        
        
        
        // Statistics
        $this->writeStatistics([
            'message' => 'Parsing was done.',
            'data' => [
                'lookups' => $this->product_lookup,
                'new' => $this->product_new
            ],
            'finish' => microtime(true),
            'time' => microtime(true) - $this->statistics['start']
        ], 'new');
        
        
        
        
        return true;
        
    }
    
    /**
     * Fill table SupplierWare.
     * Opens ALL pages and gives Category and take all wares links from
     * Receive LINK. It's current wares list link.
     * Recursive method.
     * 
    */
    private function GetSupplierWare(&$supplier_row, &$category_row, $wares_list_link)
    {
        // Stop
        if (Statistics::stopStatus('new')) {
            $this->writeStatistics($this->statsStopArray(), 'new');
            return;
        }
        
        
        // Check LINK
        if (isset($wares_list_link)) {
            if (mb_strlen($wares_list_link, 'UTF-8') > 0) {
            } else {
                return false;
            }
        } else {
            return false;
        }
        
        
        // gets HTML
        // NOTE: if web request will handle by site [?] it's can rise fatal error (no content will obtained, stop script execution)
        $html = @file_get_contents($wares_list_link);
        
        
        //  
        if (mb_strlen($html) < 50) {
            //echo 'Error: No HTML (empty page for '.$wares_list_link.').<br/>';
            return false;
        }  
        
        
        // encoding
        $html_encoding = mb_detect_encoding($html);
        if ($html_encoding != 'UTF-8') {
            $html = mb_convert_encoding($html, 'UTF-8', $html_encoding);
        }
        if (mb_detect_encoding($html) == 'UTF-8') {
        } else {
            // write: can't convert HTML Page to UTF-8 encoding
            return false;
        }
        
        
        
        
        
        // gets patterns to extract Products Block
        $pattern_row = $this->GetPattern($supplier_row['SupplierID'], null, null, 6);  
        $pattern_block_id = $pattern_row['PatternID'];  
        $pattern_block = $pattern_row['Text'];  
        
        // Get Product Block (a block with products excerpts)
        $preg_result = array();
        $wares_block_html = $this->ApplyPatternByPREG_MATCH($pattern_block, $html, $preg_result);
        
        
        
        
        
        
        
        // gets Product Link Pattern from Product Block
        $pattern_row = $this->GetPattern($supplier_row['SupplierID'], null, $pattern_block_id, 7);  
        $pattern = $pattern_row['Text'];  
        
        // applies pattern: gets Products Links
        $this->ApplyPatternByPREG_MATCH_ALL($pattern, $wares_block_html, $preg_result);
        
        
        
        
        
        
        // PREPARE SupplierWare LINK: Wares exist?, delete multiple LINKs, ...
        $checking = $this->PrepareSupplierWare_AfterPREG_MACTH_ALL($preg_result);  
        if ($checking) {
            //$prep_ware_TRUE_loops_count++;
            // writes gotted link to DB
            $is_written = $this->AddSupplierWareToDB($preg_result, $supplier_row, $category_row);
            if (! $is_written) {
                //return;
            }//*/  
            
            //return; 
        } else {
            
            // overwise continuously loop
            return false;
        }
        
        
        
        
        
        
        // get next page link
        $next_page_link = $this->GetNextSupplierWaresListLINK($supplier_row, $category_row, $category_row['LINK']);
        
        
        
        // recursive
        $this->GetSupplierWare($supplier_row, $category_row, $next_page_link);
        
    }
	
    /**
     * NOTE: this method must be customized for EVERY supplier.
    */
    private function GetNextSupplierWaresListLINK(&$supplier_row, &$category_row, $current_link)
    {
        // bind needly Supplier
        $class_name = 'Supplier_' . $supplier_row['SupplierID'];
        $supplier_obj = new $class_name();  
        $next_link = $supplier_obj->{'Processing_NextWaresListLINK'}($current_link);
        
        unset($supplier_obj);
        
        return $next_link;
    }
	
    /**
     * 
    */
    private function PrepareSupplierWare_AfterPREG_MACTH_ALL(&$preg_result)
    {
        if (count($preg_result) > 0) {} else {
            //echo 'no Wares found. -> non-existent wares list (last + 1) | change PATTERN for ...<br/>';
            // : no Categories. May be HTML-struct was changed -> check Pattern
            return false;
        }
        if (isset($preg_result[0]['link'])) {} else {
            //echo 'no feature LINK (of Ware): parser not finds it -> change pattern<br/>';
            // write note to table SupplierCategory
            return false;
        }
        
        if (mb_strlen($preg_result[0]['link'], 'UTF-8') > 1) {} else {
            //echo 'feature LINK (of Ware) are EMPTY -> change pattern<br/>';
            // write note to table SupplierCategory
            return false;    
        }
        
    
               
        
        
        $unique_link = array();
        
        $c = count($preg_result);  
        
        // check links: if uses relative links change it to full links
        for ($i = 0; $i < $c; $i++) {
            
            
            // if string contains spaces on start or end, we cut it
            $preg_result[$i]['link'] = trim($preg_result[$i]['link']);
            
            
            
            
            // ONLY unical LINKs
            $clean_link = $this->supplier->{'Processing_cleanLINK'}($preg_result[$i]['link']);
            if (is_null($clean_link)) { unset($preg_result[$i]); continue; }
            
            if (in_array($clean_link, $unique_link)) {
                unset($preg_result[$i]); continue;
            } else {
                $unique_link[] = $clean_link;
                $preg_result[$i]['link'] = $clean_link;
            }
            
            
            
            
            // replace SPECIAL CHARS: &
            /*$internal_encoding = mb_internal_encoding();
            if($internal_encoding == 'UTF-8'){} else {
                mb_regex_encoding('UTF-8');
            }
            $preg_result[$i]['link'] = mb_ereg_replace('&', "\&", $preg_result[$i]['link'], 'i');//*/
            
            
            
            
            
            // prepare link: if uses relative links, add to them Category link
            if ($preg_result[$i]['link'][0] == '/') {
                $preg_result[$i]['link'] =  $this->supplier_link . $preg_result[$i]['link'];  
            } 
        }  
        
        return true;
    }
    
    /**
     * Adds only new Supplier products, verifies by link.
     * 
     * @param $preg_result array of [Supplier Ware], each [Ware] presented only by LINK
    */
    private function AddSupplierWareToDB(&$preg_result, &$supplier_row, &$category_row)
    {
        // add ONLY new wares
        
        // Statistics
        $this->product_lookup += count($preg_result);
        $this->writeStatistics([
            'data' => [
                'lookups' => $this->product_lookup,
            ]
        ], 'new');
        
        
        
        // prepare SQL string    
        $sql_values = '';
        //$c = count($preg_result);        
        foreach ($preg_result as $key => $value) {
                        
            // if was not unique links - the $preg_result indexes is not ordinary
            //if(array_key_exists($i, $preg_result) === FALSE) { continue; }
            
            
            // CHECKING on exist in DB one current Supplier Ware
            // DEBUG table  DEBUG_SupplierWare
            //$sql = "SELECT LINK FROM DEBUG_SupplierWare WHERE LINK='" . $value['link'] . "';";
            // ORIGINAL
            $link = &$value['link']; // there can be primary SQL-injection: '0 OR 1=1', and it can hide all supplier products (or in another situation overflow database with dublicate rows)
            $this->removeSQLInjectionP($link);
            $sql = "SELECT LINK FROM SupplierWare WHERE LINK='" . $link . "';";
            
            // perform query
            $one_ware_result = $this->mysqli->query($sql);
            
            // check result of query
            if ($one_ware_result) {
                if ($one_ware_result->num_rows > 0) {
                    continue;
                } else {
                    (strlen($sql_values)==0)?'':$sql_values.=',';
                }
            } else { 
                return; 
            }
            
            
            
            /* prepare SQL string */
            $sql_values .= "(null," . $supplier_row['SupplierID'] . ",". $category_row['SupplierCategoryID'] . 
                ",'" . $value['link'] . "')";
            
            
            // New product count
            $this->product_new++;
        }  
        
        
        if (mb_strlen($sql_values, 'UTF-8') == 0) {  
            //echo 'Wares are exists for Supplier: '.$supplier_row['SiteLINK'].', Category: '.$category_row['LINK'].'.<br/>';  
            return true; 
        }  
        
        
        
        /* add new Category to DB */
        
        // DEBUG SQL for TEMPORARY  SupplierWare_debug  table
        /*$sql = 'INSERT INTO DEBUG_SupplierWare(SupplierWareID, SupplierID, SupplierCategoryID, LINK) ' .
                'VALUES '. $sql_values . ';';//*/
        
        // SQL for  SupplierWare  table
        $sql = 'INSERT INTO SupplierWare(SupplierWareID, SupplierID, SupplierCategoryID, LINK) ' .
                'VALUES '. $sql_values . ';';//*/
        
        
        $result = $this->mysqli->query($sql);
        
        if ($result === FALSE) {
            // if occurs error
            // write in Supplier table message about...
            //echo 'ERROR: until INSERT into  SupplierWare / DEBUG_SupplierWare  table.';
            return false;
        }
        
        // Statistics
        $this->writeStatistics([
            'data' => [
                'lookups' => $this->product_lookup,
                'new' => $this->product_new
            ]
        ], 'new');
        
        return true;
    }
    
    
    
    
    
    /**
     *   C A T E G O R Y
     */
    
	
	
	
	
    /**
     *   W A R E
     */
	
    
    /**
     * Reads from SupplierWare table ALL rows (: SupplierID).
     * Gets ALL Supplier wares from SupplierWare table ( AddWare() -> and scan every ware, and add it to OUR Ware table)
     * 
     * Task:    add new Supplier ware
     * Do not:  update Supplier ware features
     * 
    */
    public function AddNewWares(&$supplier_row = null)
    {
        if (empty($supplier_row))
            $supplier_row = $this->GetSupplierRow();
        
    
        // ID
        $sID = &$supplier_row['SupplierID'];
        
        
        // Statistics
        $this->statistics['message'] = 'Adding (supplier id - '.$sID.')...';
        $this->writeStatistics([
            'data' => [
                'added' => $this->product_added
            ]
        ], 'add');
        
        
        
        // get PATTERNS for ware_block and ware_feature
        $pattern_row = $this->GetPattern($sID, null, null, 8);  
        
        
        // check result
        if (! $pattern_row) {
            //echo 'Error: no pattern for Supplier ware block.';
            return false;
        }
        
        
        // get pattern
        $pattern_block_id = $pattern_row['PatternID'];
        $pattern_block = $pattern_row['Text'];  
        
        
        
        
        
        
        $pattern_feature = $this->GetPattern($sID, null, $pattern_block_id, 9, null, FALSE);
        if (! $pattern_feature) {
            //echo 'Error: no pattern for Supplier ware features (for Supplier '.$supplier_row['LINK'].').';
            return false;
        }
        $pattern_feature = $pattern_feature->fetch_all(MYSQLI_ASSOC);
        
        
        
        
        
        // get features names which to be processed by appropriate Supplier method (before write to DB)
        // returns numeric 2-dim array
        $process_feature = $this->GetProcessFeature($supplier_row);
        
              
        
          
        
        // SupplierWareID    need for compare: this ware were exist in OUR table or not
        // CategoryID        fm CategoryMap table
        /*$sql = 'SELECT SupplierWareID, SupplierCategoryID, CategoryID, LINK ' .
                'FROM SupplierWare LEFT JOIN CategoryMap USING(SupplierCategoryID) ' . 
                'WHERE SupplierID='.$supplier_row['SupplierID'] . ' ORDER BY SupplierWareID  LIMIT 10'; // LIMIT 100*/
        
        // DEBUG table  DEBUG_SupplierWare
        /*$sql =  'SELECT SupplierWareID, SupplierCategoryID, LINK ' .
                'FROM DEBUG_SupplierWare ' . 
                'WHERE SupplierID='.$supplier_row['SupplierID'] . ' ' .
                'ORDER BY SupplierWareID ' .
                //'LIMIT 1 ' . 
                ''; //*/
        // ORIGINAL table  SupplierWare
        $sql =  'SELECT SupplierWareID, SupplierCategoryID, LINK ' .
                'FROM SupplierWare ' . 
                'WHERE SupplierID='.$sID. ' ' .
                'ORDER BY SupplierWareID ' .
                //'LIMIT 100 ' . 
                '';//*/
        //if(!is_null($parent_category_id)){ $sql .= ' AND ParentCategoryID='.$parent_category_id; } else { $sql .= ' AND ParentCategoryID=0'; }
        $sql .= ';';
                
        
        // performing query
        $supplierware_table = $this->mysqli->query($sql);  
        
        // check result
        if ($supplierware_table === FALSE) {
            // arise MySQL Server error: return from method.
            //echo 'Error: problems with executing query within MySQL Server (reasons: invalid SQL query statement, server are not running, etc.).';
            return false;
        } else {
            if($supplierware_table->num_rows == 0){
                // ware exist in OUR table, go to next Supplier ware:
                //echo 'WARNING: no wares in SupplierWare table (for Supplier: '.$supplier_row['LINK'].').';
                return true;
            }
        }//*/
        
        
        
        // cycle for all wares ONE particular Supplier
        // call AddWare() which scan Supplier ware page, and extract needle features, and write its to DB table Ware.
        while ($supplierware_row = $supplierware_table->fetch_assoc()) {
            // Stop
            if (Statistics::stopStatus('add')) {
                $this->writeStatistics($this->statsStopArray(), 'add');
                return;
            }            
            
            
            
            // add new ware to OUR table Ware
            $checking = $this->AddNewWare($supplier_row, $supplierware_row, $pattern_block, $pattern_feature, $process_feature);
                    
        }
        
        
        
        // Statistics
        $this->writeStatistics([
            'message' => 'Adding was done.',
            'data' => [
                'added' => $this->product_added
            ],
            'finish' => microtime(true),
            'time' => microtime(true) - $this->statistics['start']
        ], 'add');
        
        
        return true;
    }
    
    /**
     * SELECT for SupplierID [and SupplierCategoryID]
     * 
     * TODO: later add opportunity for Category
    */
    private function GetProcessFeature(&$supplier_row)
    {
        // make query string
        $sql = "SELECT WareFeature.Name FROM SupplierWareFeatureProcessing LEFT JOIN WareFeature USING (WareFeatureID)".
                " WHERE Direction='ToDB' AND SupplierID=" . $supplier_row['SupplierID'];
        
        // if Category ID not null
        /*if(!is_null($supplier_ware_row['SupplierCategoryID'])){
            $sql .= ' AND SupplierCategoryID=' . $supplier_ware_row['SupplierCategoryID'];
        }*/
        
        // ends 
        $sql .= ';';
               
        
        
        
        $process_feature_table = $this->mysqli->query($sql);
        
        
        if ($process_feature_table) {
            
            $process_feature_array = $process_feature_table->fetch_all(MYSQLI_NUM);
            
            $process_feature_table->close();
            unset($process_feature_table);
            
            return $process_feature_array;
            
        } else {
            //echo 'Until query progress arise error (by MySQL Server). No result (no Process Features for Supplier: '.$supplier_ware_row['SupplierID'].').';
            return false;
        }
        
    }
    	
    /**
     * Load HTML page of Supplier ware
     * Apply patterns: block, features
     * Write results to DB (Ware table)
    */
    private function AddNewWare(&$supplier_row, &$supplier_ware_row, &$pattern_block, &$pattern_feature_row, &$process_feature)
    {
        // check on exist Supplier ware in OUR table
        //$sql = 'SELECT WareID FROM DEBUG_Ware WHERE SupplierWareID='.$supplier_ware_row['SupplierWareID'].';';
        // ORIGINAL table  Ware
        $sql = 'SELECT WareID FROM Ware WHERE SupplierWareID='.$supplier_ware_row['SupplierWareID'].';';
        
        
        $ware_row = $this->mysqli->query($sql);  
        
        
        if ($ware_row === FALSE) {
            // arise MySQL Server error: return from method.
            //echo 'Error: problems with executing query within MySQL Server (reasons: invalid SQL query statement, server are not running, etc.).';
            return false;
        } else {
//            if($ware_row->num_rows > 0){
//                // ware exist in OUR table, go to next Supplier ware
//                echo 'NOTICE: ware are exists in Ware table (for Supplier: '.$supplier_row['SiteLINK'].').';
//                return true;
//            }
        }  
                
               
        
        
        
        
        // prepare URL
        $supplier = $this->supplier;
        $url = $supplier->ProcessingFromDB_Link($supplier_ware_row['LINK']);
        
        // gets HTML
        $html = @file_get_contents($url);
        
        
        
        
        // DEBUG 'phpQuery' library
//        $url = __DIR__.'/product.txt'; //$url = 'https://53mission.com/women-shoes/botinki/botinki-1012321-101232-1/';
//        $html = @file_get_contents($url);
//        
//        // 'phpQuery' itself
//        global $config;
//        require dirname($config).'/../vendor/phpQuery/0.9.5.386-onefile/phpQuery-onefile.php';
//        
//        $start = microtime(true);
//        
//        // parse text and create phpDOMElement object
//        $product = phpQuery::newDocument($html);
//        // Get Product Block        
//        $product = pq( 'div.product' );
//        
//        // Product features:
//        
//        // Face Image
//        $imgFace = pq( $product )->find( 'div.pic img' )->attr( 'src' );
//        // Images
//        $images = [];
//        foreach (pq( $product )->find( 'div.smallimages img' ) as $pqDOMElement) {
//            $images[] = pq( $pqDOMElement )->attr( 'src' );
//        }
//        // Title or Name
//        $title = pq( $product )->find( 'div[itemprop=name]' )->text();
//        // Description
//        $description = pq( $product )->find( 'div[itemprop=description]' )->text();
//        // Price
//        $price = pq( $product )->find( 'span[itemprop=price]' )->text();
//        // Currency
//        $valuta = pq( $product )->find( 'span[itemprop=price] meta' )->attr('content');
//        // Sizes
//        $sizes = [];
//        foreach (pq( $product )->find( 'select[name=size] option' ) as $pqDOMElement) {
//            if (strpos($size = pq( $pqDOMElement )->text(), '---') === false) {
//                $sizes[] = $size;
//            }
//        }
//        // Description (with html, for better formatting)
//        $title2html = pq( $product )->find( 'div[itemprop=name] p' )->html();
//        // Articul
//        $articul = pq( $product )->find( 'div.status div.articul' )->text();
//        // Status
//        $status = pq( $product )->find( 'div.status div.in_stock' )->text();
//        
//        // clear memory
//        phpQuery::unloadDocuments($product);
//        
//        $finish = microtime(true);
//        
//        $total = $finish-$start;
//        echo $total; exit;
        // END DEBUG
        
        
        
        // DEBUG engine
//        $url = __DIR__.'/product.txt'; //$url = 'https://53mission.com/women-shoes/botinki/botinki-1012321-101232-1/';
//        $html = @file_get_contents($url);        
//        
//        $start = microtime(true);
        
        //  
        if (mb_strlen($html) < 50) {
            //echo 'Error: No HTML (empty page for '.$supplier_ware_row['LINK'].').<br/>';
            return false;
        }  
        
        
        
        
        // encoding
        $html_encoding = mb_detect_encoding($html);
        if ($html_encoding != 'UTF-8') {
            $html = mb_convert_encoding($html, 'UTF-8', $html_encoding);
            //if()
        }
        // check
        $html_encoding = mb_detect_encoding($html);  
        if ($html_encoding != 'UTF-8') {
            //informer: can't change encoding of page [ID Sup][LINK of page]
            return false;
        }
        
        
        
        
        
        // applies pattern: gets product block
        $preg_result = array();
        $ware_block_html = $this->ApplyPatternByPREG_MATCH($pattern_block, $html, $preg_result);
        
        
        
        // applies pattern: gets product features
        // for singular feature: string key and value
        // for non-singular feature: string key and values array
        $feature = array();
        
        $c = count($pattern_feature_row);
        
        for ($i = 0; $i < $c; $i++) {
            //
            $feature_name = $pattern_feature_row[$i]['FeatureName']; 
            if ($pattern_feature_row[$i]['ResultSingle']) {
                // if will not be feature, will be null
                $feature[$feature_name] = strip_tags( $this->ApplyPatternByPREG_MATCH($pattern_feature_row[$i]['Text'], $ware_block_html, $preg_result) );
                
            } else {
                // apply pattern. WARNING: subpattern name must have IDENTICAL name as WareFeature
                $this->ApplyPatternByPREG_MATCH_ALL($pattern_feature_row[$i]['Text'], $ware_block_html, $preg_result);
                
                // check result of PREG_MATCH_ALL: trim, add full links in $preg_result['feature_name']
                $checking = $this->PrepareSupplierWareFeatureValues_AfterPREG_MACTH_ALL($preg_result, $feature_name);
                if (! $checking) {
                    $feature[$feature_name] = null;
                    continue;
                }
                                
                // get features in array
                $feature[$feature_name] = strip_tags( $this->GetSupplierWareFeatureValues_AfterPREG_MACTH_ALL($preg_result, $feature_name) );
                
            }
        }
                
        
        
        // check and prepare features array: ALL required feature values?, custom processing features, trim
        $checking = $this->PrepareSupplierWareFeatureArray($feature, $supplier_row, $supplier_ware_row, $process_feature);      
        if (! $checking) { 
            // ware have no some REQUIRED features values  -->>  can't save to DB
            return false;
        }
        
        // DEBUG END
//        $finish = microtime(true);
//        $total = $finish-$start;
//        echo $total;
//        exit;
        
        
        
        // writes gotted features to DB
        $checking = $this->AddWareToDB($supplier_row, $supplier_ware_row, $feature);
        
        // relese memory
        unset($feature);
        
        
        if (! $checking) { 
            // save to DB (Ware table) fail.
            //echo 'Save to DB (Ware table) fail (METHOD: ' . __METHOD__ ;//. ', Supplier: '.$supplier_row['LINK'].', SupplierWare: '.$supplier_ware_row['LINK'].').<br/>';
            //return false;
        } else {
            //echo 'Saving to DB (Ware table) was success (METHOD: ' . __FUNCTION__ ;//. ', Supplier: ' . $supplier_row['LINK'] . ', SupplierWare: ' . $supplier_ware_row['LINK'] . ').<br/>';
            
            
            //return true;
        }
        
    }
    
    /**
     * Prepare  only ARRAY ITEMS (not all $feature items)
    */
    private function PrepareSupplierWareFeatureValues_AfterPREG_MACTH_ALL(&$preg_result, &$feature_name)
    {
        // check $preg_result: Categories exist?
        if (count($preg_result) > 0) {} else {
            //echo 'no Features found'."(for '".$feature_name."')".'. -> change PATTERN for ...(METHOD: ' . __METHOD__ .')<br/>';
            // : no Categories. May be HTML-struct was changed -> check Pattern
            return false;
        }
        if (isset($preg_result[0][$feature_name])) {} else {
            //echo 'no feature '.$feature_name.' (of Ware)(METHOD: ' . __METHOD__ .')<br/>';
            // write note to table SupplierCategory
            return false;
        }
        
         if (mb_strlen($preg_result[0][$feature_name], 'UTF-8') > 0) {} else {
            //echo 'feature '.$feature_name.' of Ware are EMPTY (METHOD: ' . __METHOD__ . ')<br/>';
            // write note to table SupplierCategory
            return false;    
        }
        
        
            
        $c = count($preg_result);
        
        // check links: if uses relative links change it to full links
        for ($i = 0; $i < $c; $i++) {
            // if string contains spaces on start or end, we cut it
            $preg_result[$i][$feature_name] = trim($preg_result[$i][$feature_name]);
                        
            // prepare link: if uses relative links, add to them site link
            if ($preg_result[$i][$feature_name][0] == '/' && $preg_result[$i][$feature_name][1] != '/') {
                $preg_result[$i][$feature_name] = $this->supplier_link . $preg_result[$i][$feature_name];
            }
        }
        
        return true;
    }
    
    /**
     * Return array (found feature values). No trim values.
    */
    private function GetSupplierWareFeatureValues_AfterPREG_MACTH_ALL(&$preg_result, &$feature_name)
    {
        // prepare feature values array
        $values = array();        
        $c = count($preg_result);
        for ($i = 0; $i < $c; $i++) {
            // if string contains spaces on start or end, we cut it
            $values[] = $preg_result[$i][$feature_name];
        }
        
        return $values;
    }
    
    /**
     * 
     * After it method FEATURES are full prepared
    */
    private function PrepareSupplierWareFeatureArray(&$feature, &$supplier_row, &$supplier_ware_row, &$process_feature)
    {
        // FEATURES EXISTS?
        if (! (count($feature) > 0)) {
            //echo 'no one items in feature array -> change PATTERNs for them (<br/>Supplier: '.$supplier_row['LINK'].', SupplierWare: '.$supplier_ware_row['LINK'].').<br/>';
            // : notify to DB
            return false;
        }
        
        
        // check all REQUIRED features and them values
        if (! (isset($feature['Title']) AND (strlen($feature['Title']) > 0))) {
            
            return false;
        }
        
        
        
        
        // Prepare values: CUSTOM PROCESSING: prepare some feature values to valid (for DB) form.
        $c = count($process_feature);
        if ($c > 0) {
            // bind needly Supplier
            $class_name = 'Supplier_' . $supplier_row['SupplierID'];
            $supplier_obj = new $class_name();
                        
            // process features represent like first value of numeric array
            for ($i = 0; $i < $c; $i++) {
                $feature_name = $process_feature[$i][0];
                                        
                if (array_key_exists($feature_name,$feature) AND isset($feature[$feature_name])) {
                                        
                    // WARNING: method can receive parameters - WITHOUT them him not start -> arise fatal error
                    if (! method_exists($supplier_obj, 'ProcessingToDB_'.$feature_name)) {
                        // for 
                        //echo "Error: no process method '".'ProcessingToDB_'.$feature_name."' for Supplier: ".$supplier_row['SupplierID'].".<br/>";
                        continue;
                    }
                    
                    $feature_value = $supplier_obj->{'ProcessingToDB_'.$feature_name}($feature[$feature_name]);
                    //$this->removeSQLInjectionP($feature_value); // only for LINKs, overwise it concatenate all words in one word
                    $feature[$feature_name] = $feature_value;
    
                }
                
            }
            
            unset($supplier_obj);
        }
                
        
        // Check & Prepare  ALL feature values
        foreach ($feature as $key => &$value) {
            
            //  UNHANDLED value: array
            if (is_array($value)) {
                //echo "WARNING: value of key '".$key."' is array. It's can't to be write to DB.<br/>";
                exit('FATAL ERROR');
            }
            //  UNHANDLED value: object
            if (is_object($value)) {
                //echo "WARNING: value of key '".$key."' is object. It's can't to be write to DB.<br/>";
                exit('FATAL ERROR');
            }
            
            
            /*// change ENCODING to UTF-8 if other is set
            
            $encoding = mb_detect_encoding($value);
            
            if((!mb_check_encoding($value, 'UTF-8')) OR ($encoding != 'UTF-8')){
                
                $value = mb_convert_encoding($value, 'UTF-8', $encoding);
                
            }*/
            
            
            
            
            // delete all SINGLE QUOTES
            $value = str_replace("'", '', $value);
            
            
            // TRIM VALUES
            $value = trim($value);
            
            
            
            // FULL LINK
            if (is_string($value) AND strlen($value) > 0) {
                if ($value[0] == '/') { $value = $this->supplier_link . $value; }
            }
            
        }
        
        return true;
    }
    
    /**
     * There can be primary SQL-injection: '0 OR 1=1', and it can hide all supplier
     * products (or in another situation overflow database with dublicate rows).
     */
    private function removeSQLInjection($value)
    {
        $remove = ['%20', ' ', '>', '<', '=', '!', ';', ','];
        return str_replace($remove, '', $value);
    }
    private function removeSQLInjectionP(&$value)
    {
        $remove = ["'", '%20', ' ', '>', '<', '=', '!', ';', ','];
        $value = str_replace($remove, '', $value);
    }
    
    /**
    */
    private function AddWareToDB(&$supplier_row, &$supplier_ware_row, &$feature)
    {
        // prepare values
        $sql_field = array();
        
        // Supplier object
        $supplier = 'Supplier_'.$supplier_row['SupplierID'];
        $supplier = new $supplier();        
        
        
        
        
        
        
        // prepare SQL string    
        
        // start transaction
        $result = $this->mysqli->query("START TRANSACTION;");
        //$result1 = $this->mysqli->query("SET autocommit=0;");
        //$result2 = $this->mysqli->query("LOCK TABLES Escala.DEBUG_Ware WRITE;");
        //$result3 = $this->mysqli->query("LOCK TABLES information_schema.TABLES READ;");
        
        //$this->mysqli->query("ROLLBACK;");
        if($result === FALSE){ 
            $this->mysqli->query("ROLLBACK;");
            //echo 'Error: until request was process arise MySQL Server error (START TRANSACTION).';
            return false; 
        }
        /*if(!$result1){ 
            echo 'Error: until request1 was process arise MySQL Server error (START TRANSACTION).';
            return false; 
        }
        if(!$result2){ 
            echo 'Error: until request2 was process arise MySQL Server error (START TRANSACTION).';
            return false; 
        }
        if(!$result3){ 
            echo 'Error: until request3 was process arise MySQL Server error (START TRANSACTION).';
            return false; 
        }*/
        
        // get current auto_icrement
        $auto_increment = '';
        
        // DEBUG table  DEBUG_Ware
        //$result_select = $this->mysqli->query("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA='escala' AND TABLE_NAME='DEBUG_Ware' LIMIT 1 FOR UPDATE;");
        // ORIGINAL table  Ware
        $result_select = $this->mysqli->query("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_SCHEMA='".$this->db."' AND TABLE_NAME='Ware' LIMIT 1 FOR UPDATE;");
        
        //$this->mysqli->query("ROLLBACK;");
        if ($result_select AND ($result_select->num_rows > 0)) {
            
            $auto_increment_row = $result_select->fetch_array(MYSQLI_ASSOC);
            $auto_increment = $auto_increment_row["AUTO_INCREMENT"];
            
        } else { 
            $this->mysqli->query("ROLLBACK");
            //echo 'Error: until request was process arise MySQL Server error (START TRANSACTION -> SELECT AUTO_INCREMENT).';
            return false; 
        }
        
               
        
        // (OUR fields) identifying information (in our DB)
        //$sql_field['WareID'] = null; // allow MySQL set DEFAULT value for this field
        $sql_field['SupplierID'] = [ 'val' => &$supplier_row['SupplierID'], 'type' => 'i'];
        $sql_field['SupplierCategoryID'] = [ 'val' => &$supplier_ware_row['SupplierCategoryID'], 'type' => 'i' ];
        $sql_field['SupplierWareID'] = [ 'val' => &$supplier_ware_row['SupplierWareID'], 'type' => 'i' ];
        
        // Articul
        $articul = &$feature['Articul'];
        $articul = (isset($articul) AND mb_strlen($articul, 'UTF-8') > 0 )? $articul : ''; 
        $articul = (mb_strlen($articul, 'UTF-8') > 0) ? $articul : null;
        if (! empty($articul))
            $sql_field['Articul'] = [ 'val' => &$articul, 'type' => 's' ];
        
        // Prom.ua fields
        
        // Code (max 15)
        $code = $feature['Title'];
        $code_tail = (mb_strlen($articul, 'UTF-8') > 0) ? ' '.$articul : '';            //$supplier_row['SupplierID'].'-'.$supplier_ware_row['SupplierCategoryID'];//.'-'.$supplier_ware_row['SupplierWareID'];
        $code_tail_length = mb_strlen($code_tail, 'UTF-8');
        if (mb_strlen($code, 'UTF-8') > (15-$code_tail_length)) {
            $code = mb_substr($code, 0, 15-$code_tail_length, 'UTF-8');
        }
        $code .= $code_tail;
        if (! empty($code))
            $sql_field['Code'] = [ 'val' => &$code, 'type' => 's' ];
        
        
        
        // Title  (max 110)
        $title = $feature['Title'];
        if (mb_strlen($title, 'UTF-8') > 110) { 
            $title = mb_substr($title, 0, 110, 'UTF-8'); 
        }
        $sql_field['Title'] = [ 'val' => &$title, 'type' => 's' ]; // +
        
        
        
        // Title2  (max 110)
        $title2 = &$feature['Title2'];
        if (isset($title2) AND (mb_strlen($title2, 'UTF-8') > 110)) { 
            $title2 = mb_substr($title2, 0, 110, 'UTF-8'); 
        }
        $title2 = (isset($title2) AND (mb_strlen($title2, 'UTF-8') > 0)) ? $title2 : null;
        if (! empty($title2))
            $sql_field['Title2'] = [ 'val' => &$title2, 'type' => 's' ];
        
        
        
        
        // Key words  (max 255)
        $key_words = '';
        if (($title2 != null) AND ($title2 != $feature['Title'])) { 
            $key_words .= $title2.', ';
        }
        $key_words .= $feature['Title']; // + Category Name + ... + Top Category Name
        if (mb_strlen($key_words, 'UTF-8') > 255) { 
            $key_words = mb_substr($key_words, 0, 255, 'UTF-8'); 
        } 
        if (! empty($key_words))
            $sql_field['KeyWords'] = [ 'val' => &$key_words, 'type' => 's' ];
        
        
        
        
        // Description  (max 10 000)
        $description = &$feature['Description'];
        if (isset($description) AND (mb_strlen($description, 'UTF-8') > 10000)) { 
            $description = mb_substr($description, 0, 10000, 'UTF-8'); 
        }
        $description = (isset($feature['Description']) AND (mb_strlen($feature['Description'], 'UTF-8') > 0)) ? $feature['Description'] : "'none'";
        $sql_field['Description'] = [ 'val' => &$description, 'type' => 's' ]; // +
        
        
        
        
        // 'Тип' товара: оптом, в розницу, оптом и в розницу
        $sql_field['WareType'] = [ 'val' => 'u', 'type' => 's' ];
        
        
        
        
        // Цена
        $price = &$feature['Price'];
        if (isset($price) AND (mb_strlen($price, 'UTF-8') > 0) AND is_numeric($price)) {
            $price = $price + 100; // COMPUTING PRICE
        } else {
            $price = null;
        }
        if (! empty($price))
            $sql_field['Price'] = [ 'val' => &$price, 'type' => 'i' ];
        
        if ($sql_field['Price'] == null) {
//            $sql_field['Valuta'] = [ 'val' => null, 'type' => 's' ];
//            $sql_field['MeasureUnit'] = [ 'val' => null, 'type' => 's' ];
//            $sql_field['Discount'] = [ 'val' => null, 'type' => 's' ]; 
        } else {
            $valuta = &$feature['Valuta'];
            $valuta = (isset($valuta) AND (mb_strlen($valuta, 'UTF-8') > 0)) ? $valuta : null; // DEFAULT=UAH
            if (! empty($valuta))
                $sql_field['Valuta'] = [ 'val' => $valuta, 'type' => 's' ];
            //$sql_field['MeasureUnit'] = [ 'val' => null, 'type' => 's' ]; //DEFAULT='шт.'
            
            $discount = &$feature['Discount'];
            $discount = (isset($discount) AND (mb_strlen($discount, 'UTF-8') > 0)) ? $discount : null; // DEFAULT=null
            if (! empty($discount))
                $sql_field['Discount'] = [ 'val' => $discount, 'type' => 's' ];
            //$sql_field['MinQuantOrder'] = [ 'val' => null, 'type' => 'i' ]; //null; // DEFAULT=1 
            
            // для оптовых товаров/покупателей
            $price = $price + 10; // COMPUTING WHOLESALE PRICE
            //$price = "'".$price."'";
            $sql_field['WholesalePrice'] = [ 'val' => $price, 'type' => 'd' ]; //null;
            $sql_field['MinWholesaleOrder'] = [ 'val' => "'3'", 'type' => 'd' ];//"'3'";////null;
            //$sql_field['DeliveryQuantity'] = [ 'val' => null, 'type' => 'i' ];
            //$sql_field['DeliveryPeriod'] = [ 'val' => null, 'type' => 's' ];
            //$sql_field['PackagingWay'] = [ 'val' => null, 'type' => 's' ];
            
        }
        
        
        
        
        
        // Images
        $imgFace = &$feature['ImageFace'];
        $imgFace = (isset($imgFace)) ? $imgFace : null;
        if (! empty($imgFace))
            $sql_field['ImageFace'] = [ 'val' => &$imgFace, 'type' => 's' ];
        
        $img = &$feature['Image'];
        $img = (isset($img)) ? $img : null;
        if (! empty($img))
            $sql_field['Image'] = [ 'val' => &$img, 'type' => 's' ];
        
        
        
        // Наявность
        $availability = &$feature['Availability'];
        $availability  = (isset($availability) AND (mb_strlen($availability, 'UTF-8') > 0)) ? $availability : null; // DEFAULT='+'
        if (! empty($availability))
            $sql_field['Availability'] = [ 'val' => &$availability, 'type' => 's' ];
        
        
        
        // Название производителя
        $manufacturer = &$feature['Manufacturer'];
        if (isset($manufacturer) AND (mb_strlen($manufacturer, 'UTF-8') > 0)) {
        } else {
            $manufacturer = $supplier::MANUFACTURER; // must BE declare and SET in Supplier Class definition
        } 
        if (mb_strlen($manufacturer, 'UTF-8') > 255) {
            $manufacturer = mb_substr($manufacturer, 0, 255, 'UTF-8'); 
        }
        $sql_field['Manufacturer'] = [ 'val' => &$manufacturer, 'type' => 's' ];
                
        
        
        // Название страны производителя
        $manufacturer_country = &$feature['CountryManufacturer'];
        if (isset($manufacturer_country) AND (mb_strlen($manufacturer_country, 'UTF-8') > 0)) {
        } else {
            $manufacturer_country = $supplier::MANUFACTURER_COUNTRY; // must BE declare and SET in Supplier Class definition
        }
        if (mb_strlen($manufacturer_country, 'UTF-8') > 30) { 
            $manufacturer_country = mb_substr($manufacturer_country, 0, 30, 'UTF-8'); 
        }
        $sql_field['CountryManufacturer'] = [ 'val' => &$manufacturer_country, 'type' => 's' ];
        
        
        
                
        // other fields
        //$sql_field['GroupNumber'] = null;  DELETED, uses LEFT JOIN with Category table insted
        //$sql_field['PromCategoryLINK'] = null; // ПРОМОм, автоматически на основе ключевых слов
        $sql_field['WareIdentifier'] = [ 'val' => 'w_'.$auto_increment, 'type' => 's' ]; //null;// +  MAKE in Supplier_* class until exporting
        //$sql_field['UnicalIdentifier'] = [ 'val' => null, 'type' => 's' ];
        //$sql_field['PromSubCategoryID'] = null;
        //$sql_field['OurSubCategoryID'] = null;//(isset($supplier_ware_row[''])) ? 0;
        $sql_field['WareGroupID'] = [ 'val' => &$auto_increment, 'type' => 'i' ]; // ?: ware face | ware
        
        
        
        
        // (OUR fields) Additional features fields
        // Size
        $size = &$feature['Size'];
        if (isset($size)) {
            if (mb_strlen($size, 'UTF-8') > 300) {
                $size = mb_substr($size, 0, 300, 'UTF-8');
            }
        }
        $size = (isset($size)) ? $size : null; //DEFAULT=null
        if (! empty($size))
            $sql_field['Size'] = [ 'val' => &$size, 'type' => 's' ];
        
        
        
        
        
        // Color
        $color = &$feature['Color'];
        if (isset($color)) {
            if (mb_strlen($color, 'UTF-8') > 300) {
                $color = mb_substr($color, 0, 150, 'UTF-8');
            }
        }
        $color = (isset($color)) ? $color : null; // DEFAULT=null
        if (! empty($color))
            $sql_field['Color'] = [ 'val' => &$color, 'type' => 's' ];
        
        
        
        
        
        // (OUR fields) 
        //$sql_field['ExistsByLINK'] = [ 'val' => null, 'type' => 'i' ];
        //$sql_field['Hidden'] = [ 'val' => null, 'type' => 'i' ];
        //$sql_field['CreatedTIME'] = [ 'val' => null, 'type' => 's' ];
        // [ 'val' => 'DEFAULT', 'type' => 's' ]
        
        
        // CLEAR memory
        unset($supplier);
   
        
        
        
        
        // DEBUG table  DEBUG_Ware
        // LOCK  table  DEBUG_Ware  for reading/writing
        /*$result_LOCK = $this->mysqli->query('SELECT 1 FROM DEBUG_Ware LIMIT 1  FOR UPDATE;');
        $sql = 'INSERT INTO DEBUG_Ware(' .$sql_fields. ') VALUES (' . $sql_values . ');';//*/
        
        // ORIGINAL table  Ware
        // LOCK  table  Ware  for reading/writing
        $result_LOCK = $this->mysqli->query('SELECT 1 FROM Ware LIMIT 1  FOR UPDATE;');
        if ($result_LOCK === FALSE) { 
            $this->mysqli->query("ROLLBACK;");
            //echo 'Error: until request was process arise MySQL Server error (result_LOCK::SELECT * FROM Ware).';
            return false; 
        }
        
        
        
        
        
        // add new Ware (with all found Features) to our table  
        // cycle for all features
        $sql_fields = '';
        $sql_params = '';
        $sql_types = '';
        $sql_values = [];
        
        $sql_values[0] = ''; // reserve place for $sql_types
        
        foreach ($sql_field as $field_name => $field_value_arr) {
            
            // fields
            $sql_fields .= $field_name . ', ';
            
            // params
            $sql_params .= '?, ';
            
            // types
            $sql_types .= $field_value_arr['type'];
            
            // values
            $sql_values[] = &$field_value_arr['val'];
            
            //if($field_name != 'CreatedTIME'){ $sql_fields .= ', '; $sql_values .= ', '; }
            
        }
        $sql_fields = substr($sql_fields, 0, -2);
        $sql_params = substr($sql_params, 0, -2);
        $sql_values[0] = &$sql_types;
        
        $sql = 'INSERT INTO Ware(' .$sql_fields. ') VALUES (' . $sql_params . ');';//*/
        $stmt = $this->mysqli->prepare($sql);
        $bind_result = call_user_func_array([$stmt, 'bind_param'], $sql_values);
        $result_INSERT = $stmt->execute(); //   M A I N   I N S E R T
        $stmt->affected_rows;
                
        
        
        if ($result_INSERT) {
            
            $this->mysqli->query("COMMIT;");  
            
            
            // Statistics
            $this->product_added++;
            $this->writeStatistics([
                'data' => [
                    'added' => $this->product_added,
                ]
            ], 'add');
            
        } else {        
            $this->mysqli->query("ROLLBACK");
            //echo 'Error: until request was process arise MySQL Server error (INSERT into Ware table, SupplierWareID: '.$supplier_ware_row['SupplierWareID'].'). <br/>';
            //echo 'SQL (SupplierWareID: '.$supplier_ware_row['SupplierWareID'].'): '.$sql.'<br/>---<br/>';
            return false;
        }
                
        
        //$this->mysqli->query("UNLOCK TABLES;");
        
        
        return true;    
    }
    
    /**
     * Product name + Product Number in our Category
     * 
    */
    private function GetWareCode(&$supplier_row, &$supplier_ware_row)
    {
        static $supplier = 0;
        static $category = 0;
    }
    
    /**
     * Product name + Category Name + ... + Top Category Name
     * 
    */
    private function GetWareKeyWords(&$supplier_row, &$supplier_ware_row)
    {
        
    }
    
	
	

    
    /**
     *  P A T T E R N
     */
	
    /**
     * Pattern
     * 
     * @param type $supplier_id
     * @param type $supplier_category_id
     * @param type $parent_pattern_id
     * @param type $pattern_type_id
     * @param type $ware_feature_id
     * @param type $single_row
     * @return boolean
     */
    public function GetPattern($supplier_id, $supplier_category_id = null, $parent_pattern_id = null, $pattern_type_id, $ware_feature_id = null, $single_row = true)
    {
        // Pattern, WareFeature
        $sql_fields = 'PatternID, ResultSingle, Text, WareFeature.Name AS FeatureName';
        
        // Pattern  < WareFeature
        $sql_tables = 'Pattern LEFT JOIN WareFeature USING(WareFeatureID)';
  		
        
        // Conditions
        $sql_cond = 'Pattern.SupplierID='.$supplier_id;
        if (! is_null($supplier_category_id)) { $sql_cond .= ' AND Pattern.SupplierCategoryID='.$supplier_category_id; }
        if (! is_null($parent_pattern_id)) { $sql_cond .= ' AND ParentPatternID='.$parent_pattern_id; }
        if (! is_null($pattern_type_id)) { $sql_cond .= ' AND PatternTypeID='.$pattern_type_id; }
        if (! is_null($ware_feature_id)) { $sql_cond .= ' AND Pattern.WareFeatureID='.$ware_feature_id; }
        
        
        // final SQL query string
        $sql = 'SELECT '.$sql_fields.' FROM '.$sql_tables. ' WHERE '. $sql_cond . ' ORDER BY PatternID;';;
        
        // performing Query
        if ($result = $this->mysqli->query($sql)) {
            if ($single_row AND $result->num_rows > 0) {
                $result_row = $result->fetch_assoc();
                $result->close(); 
                return $result_row;
            } else {
                return $result;
            }
        }
        //echo $sql;
        return false;
    }
    
    /**
     * Prepare SPLACED pattern for ALL features of ware
    */
    private function GetWareFeaturesPattern(&$patterns)
    {
        // check Patterns
       if (! ($patterns instanceof mysqli_result)) {
            // ..
            //echo '<br/>Can|t splice patterns (for ware features): given object is no mysqli_result.<br/>';
            return '';        
       }
       if ($patterns->num_rows < 1) {
            //echo '<br/>Can|t splice patterns (for ware features): no rows in given object.<br/>';
            return '';
       }
       
       
       $features_pattern = '';
       
       while ($feature_pattern_row = $patterns->fetch_assoc()) {
            
            if ($feature_pattern_row['Before0+'] == 1) { 
                $features_pattern .= '.*'; 
            } elseif ($feature_pattern_row['Before1+'] == 1) { 
                $features_pattern .= '.+'; 
            }
            
            $features_pattern .= $feature_pattern_row['Text'];
            
            if ($feature_pattern_row['After0+'] == 1) { 
                $features_pattern .= '.*'; 
            } elseif ($feature_pattern_row['After1+'] == 1) { 
                $features_pattern .= '.+'; 
            }
            
        }
        
        return $features_pattern;
    }
        
    /**
    */
    private function ApplyPatternByPREG_MATCH(&$pattern, &$html, &$preg_result)
    {
	/* prepare pattern (for get Category block)) */
        $reg_exp_modifiers = 'isU';
        $reg_exp = '/' . $pattern . '/' . $reg_exp_modifiers;
        preg_match($reg_exp, $html, $preg_result);  
        
        // check result
        // NOTE: $preg_result ideally are array with 2 items: first item is full html which complies to pattern, second item
        // is  needly SUBPATTERN - block, Category name, feature
        if (count($preg_result) > 1  AND  mb_strlen($preg_result[1], 'UTF-8') > 0) {
        } else {
            // note in Supplier table that parser find nothing -> check pattern (for this Supplier)
            return null;
        }
        
        
        /* get html of top category block: $preg_result[0] - full block, $preg_result[1] - () */
	return $preg_result[1];
    }
	
    /**
    */
    private function ApplyPatternByPREG_MATCH_ALL(&$pattern, &$html, &$preg_result)
    {
        /* prepare pattern (for get Categories from Category block)) */
        $reg_exp_modifiers = 'isU';
        $reg_exp = '/' . $pattern . '/' . $reg_exp_modifiers;  
        preg_match_all($reg_exp, $html, $preg_result, PREG_SET_ORDER);  
        // PREG_SET_ORDER - размещает входжение () в одной строке (в эл-тах одного массива)
    }
}
