<?php

class Statistics
{
    public static function getDir()
    {
        global $config;
        return dirname($config) . '/../data';
    }
    public static function getData(string &$action)
    {
        $path = self::getDir() . '/statistics-' . $action . '.json';
        if (file_exists($path))
            return file_get_contents($path);
    }
    public static function getDataArray(string &$action)
    {
        $path = self::getDir() . '/statistics-' . $action . '.json';
        if (file_exists($path)) {
            $json = file_get_contents($path);
            return json_decode($json, true);
        }
    }
    public static function setData($action, array &$data)
    {
        $json = json_encode($data, JSON_PRETTY_PRINT);
        file_put_contents(self::getDir() . '/statistics-' . $action . '.json', $json);
    }
    public static function stopAction($action)
    {
        $data = [
            'stop' => true
        ];
        file_put_contents(self::getDir() . '/statistics-' . $action . '-stop.json', json_encode($data));
    }
    public static function clear($action)
    {
        $path = self::getDir() . '/statistics-' . $action . '.json';
        if (file_exists($path))
            unlink($path);
    }
    public static function stopOff($action)
    {
        $path = self::getDir() . '/statistics-' . $action . '-stop.json';
        if (file_exists($path))
            unlink($path);
    }
    public static function stopStatus($action)
    {
        $path = self::getDir() . '/statistics-' . $action . '-stop.json';
        if (file_exists($path)) {
            $status = json_decode(file_get_contents($path), true)['stop'];
            return $status;
        }
        return false;
    }
}
